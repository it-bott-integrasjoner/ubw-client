# Client for the UBW SOAP and REST APIs

## Example using REST API

```python
#!/usr/bin/env python

from ubw_client.client import get_client

client = get_client(rest={'headers': {'X-Gravitee-Api-Key': '...',
                                      'X-Some-Other-Header': '...'},
                          'base_url': 'https://apigw-dev.dfo.no/dev/v1/', #  https://dfo-devtest.azure-api.net/
                          'endpoints': {'project': {
                                            'headers': {'X-Gravitee-Api-Key': 'key'}},
                                        'begreper': {
                                            'headers': {'X-Gravitee-Api-Key': 'key'}},
                                        'begrepsverdier': {
                                            'headers': {'X-Gravitee-Api-Key': 'key'}},
                                        'periode': {
                                            'headers': {'X-Gravitee-Api-Key': 'key'}}, #  Ocp-Apim-Subscription-Key
                                        'firma': {
                                            'headers': {'X-Gravitee-Api-Key': 'key'}},
                                        'bilagstyper': {
                                            'headers': {'X-Gravitee-Api-Key': 'key'}},
                                        'ressurser': {
                                            'headers': {'X-Gravitee-Api-Key': 'key'}}}},
                    import_service={'credentials': {'client': 1,
                                                    'password': '<password>',
                                                    'username': '<username>'},
                                    'headers': {'X-Gravitee-Api-Key': '<grav-key-imp-service>'},
                                    'wsdl': 'https://agr-bott-utv02.sfso.no/AgrBOTT_Utv02_WS/service.svc?ImportService/ImportV200606',
                                    'alternate_endpoint': 'https://dfo-devtest.azure-api.net/dev/soap/importtjeneste'}
                    transaction_service={'credentials': {'client': 1,
                                                         'password': '<password>',
                                                         'username': '<username>'},
                                         'headers': {
                                             'X-Gravitee-Api-Key': '<grav-key-trx-service>'},
                                         'wsdl': 'https://agr-bott-utv02.sfso.no/AgrBOTT_Utv02_WS/service.svc?BatchInputService/BatchInput',
                                         'alternate_endpoint': 'https://dfo-devtest.azure-api.net/dev/soap/transaksjoner'})

b = client.get_begreper(72)
print(b[0])

p = client.get_prosjekter("110001")
print(p)
```

## Example using SaveTransactions

```python
from ubw_client import models
from ubw_client import get_client


trans_list = [{"account": "3030",
               "amount": "456",
               "arrival_date": "2020-06-23",
               "compl_delay": "2020-06-23",
               "disc_date": "2020-06-23",
               "due_date": "2020-06-23",
               "trans_date": "2020-06-23",
               "voucher_date": "2020-06-23",
               "client": "72",
               "cur_amount": "456",
               "currency": "NOK",
               "description": "BatchInput Test",
               "dim1": "20000000",
               "dim2": "110000",
               "dim5": "110000100",
               "interface": "BI",
               "sequence_no": 0,
               "tax_code": "0",
               "trans_type": "GL",
               "voucher_no": "1110",
               "voucher_type": "HB",
               "ex_ref": "7272GK1-OS"
               },
              {"account": "3030",
               "amount": "789",
               "arrival_date": "2020-06-23",
               "compl_delay": "2020-06-23",
               "disc_date": "2020-06-23",
               "due_date": "2020-06-23",
               "trans_date": "2020-06-23",
               "voucher_date": "2020-06-23",
               "client": "72",
               "cur_amount": "789",
               "currency": "NOK",
               "description": "BatchInput Test",
               "dim1": "20000000",
               "dim2": "110000",
               "dim5": "110000100",
               "interface": "BI",
               "sequence_no": 0,
               "tax_code": "0",
               "trans_type": "GL",
               "voucher_no": "1111",
               "voucher_type": "HB",
               "ex_ref": "7272GK1-OS"
               }
              ]


c = get_client(...)

transactions = models.Transactions.parse_obj(trans_list)
resp = c.save_transactions(transactions, ('interface', 'BI'), 0, 202006, "")
```

## Code style

### Black

This project uses [black](https://github.com/psf/black/) to format the code.
See [black documentation](https://black.readthedocs.io/en/stable/editor_integration.html) for how to set up _black_ with your editor.

To autoformat all code, run

```shell script
git ls-files '*.py' | xargs black
```

[Flake8](https://flake8.pycqa.org/en/latest/) is also used as a linter.

Code style errors will result in CI pipeline failing.

You can use a `pre-commit` hook to have git run linters before commit.
Save this to `.git/hooks/pre-commit` and make it executable.

```sh
#!/bin/sh

tox -e linters
```

## Tests

To run all tests

```shell script
tox
```

You can use a `pre-push` hook to have git run all tests before pushing.
Save this to `.git/hooks/pre-push` and make it executable.

```sh
#!/bin/sh

tox
```

### Unit tests

```sh
python -m pytest
```

### Coverage
```sh
coverage run --branch --source=ubw_client -m pytest && coverage report --show-missing
```

### Integration tests

Set credentials in `config.yaml` and run

```sh
python -m pytest -m integration
```

### Stresstesting

Set credentials in `config.yaml` and run

```sh
locust -f locust_testing/stresstest.py
```
then open http://127.0.0.1:8089/ and choose the desired parameters of the test, as 
well as the base url for the server that is to be tested.

See [locust documentation](https://docs.locust.io/en/stable/)
