from ubw_client.client import get_rest_client


def test_prosjekter_url_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"
    config_no_overrides["rest"]["endpoints"]["project"][
        "url"
    ] = "https://example.com/nonbase/prosjekter/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_prosjekter("5")
    assert url == "https://example.com/nonbase/prosjekter/5"


def test_prosjekter_url_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"
    config_no_overrides["rest"]["endpoints"]["project"][
        "url"
    ] = "https://example.com/nonbase/prosjekter"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_prosjekter("5")
    assert url == "https://example.com/nonbase/prosjekter/5"


def test_bruker_url_no_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bruker("72", "5")
    assert url == "https://example.com/base/brukere/72/5"


def test_bruker_url_no_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bruker("72", "5")
    assert url == "https://example.com/base/brukere/72/5"


def test_bruker_url_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"
    config_no_overrides["rest"]["endpoints"]["bruker"][
        "url"
    ] = "https://example.com/nonbase/brukere/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bruker("72", "5")
    assert url == "https://example.com/nonbase/brukere/72/5"


def test_bruker_url_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"
    config_no_overrides["rest"]["endpoints"]["bruker"][
        "url"
    ] = "https://example.com/nonbase/brukere"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bruker("72", "5")
    assert url == "https://example.com/nonbase/brukere/72/5"


def test_brukere_url_no_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bruker("72")
    assert url == "https://example.com/base/brukere/72"


def test_brukere_url_no_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bruker("72")
    assert url == "https://example.com/base/brukere/72"


def test_brukere_url_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"
    config_no_overrides["rest"]["endpoints"]["bruker"][
        "url"
    ] = "https://example.com/nonbase/brukere/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bruker("72")
    assert url == "https://example.com/nonbase/brukere/72"


def test_brukere_url_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"
    config_no_overrides["rest"]["endpoints"]["bruker"][
        "url"
    ] = "https://example.com/nonbase/brukere"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bruker("72")
    assert url == "https://example.com/nonbase/brukere/72"


def test_konto_url_no_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_konto("72", "2942")
    assert url == "https://example.com/base/kontoplan/v1/72/2942"


def test_konto_url_no_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_konto("72", "2942")
    assert url == "https://example.com/base/kontoplan/v1/72/2942"


def test_kontoplan_url_no_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_kontoplan("72")
    assert url == "https://example.com/base/kontoplan/v1/72"


def test_kontoplan_url_no_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_kontoplan("72")
    assert url == "https://example.com/base/kontoplan/v1/72"


def test_konteringsregler_url_no_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_konteringsregler("72")
    assert url == "https://example.com/base/konteringsregler/v1/72"


def test_konteringsregler_url_no_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_konteringsregler("72")
    assert url == "https://example.com/base/konteringsregler/v1/72"


def test_konteringsregel_url_no_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_konteringsregel("72", "2")
    assert url == "https://example.com/base/konteringsregler/v1/72/2"


def test_konteringsregel_url_no_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_konteringsregel("72", "2")
    assert url == "https://example.com/base/konteringsregler/v1/72/2"


def test_bilagstyper_url_no_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bilagstype("72", "TT")
    assert url == "https://example.com/base/bilagstyper/v1/72/TT"


def test_bilagstyper_url_no_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_bilagstype("72", "TT")
    assert url == "https://example.com/base/bilagstyper/v1/72/TT"


def test_ressurser_url_no_override_w_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base/"  # trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_ressurser("72", "2")
    assert url == "https://example.com/base/ressurser/v1/72/2"


def test_ressurser_url_no_override_wo_trailing_slash(config_no_overrides):
    config_no_overrides["rest"]["base_url"] += "base"  # no trailing slash
    client = get_rest_client(**config_no_overrides)
    url = client.urls.get_ressurser("72", "2")
    assert url == "https://example.com/base/ressurser/v1/72/2"
