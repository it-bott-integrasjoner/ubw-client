import datetime
import json
from typing import Any, Dict, KeysView, List, Optional, Type, TypeVar, Union
from base64 import b64decode

import pydantic
import enum


def to_lower_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8"""
    first, *others = s.split("_")
    return "".join([first.lower(), *map(str.capitalize, others)])


def to_upper_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8"""
    parts = s.split("_")
    return "".join(map(str.capitalize, parts))


def decapitalize_first_char(s: str) -> str:
    f = s[0]
    r = s[1:]
    return "".join([f.lower(), r])


T = TypeVar("T", bound="BaseModel")


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls: Type[T], data: Dict[Any, Any]) -> T:
        return cls(**data)

    @classmethod
    def from_json(cls: Type[T], json_data: str) -> T:
        data = json.loads(json_data)
        return cls.from_dict(data)

    def dict(
        self,
        by_alias: bool = True,
        exclude_unset: bool = True,
        *args: Any,
        **kwargs: Any,
    ) -> Dict[Any, Any]:
        return super().dict(
            by_alias=by_alias, exclude_unset=exclude_unset, *args, **kwargs
        )

    def keys(self) -> KeysView[str]:
        return self.__fields__.keys()

    def __getitem__(self, item: str) -> Any:
        return self.__getattribute__(item)


class UbwSoapCredentials(BaseModel):
    Username: str
    Client: str
    Password: str

    class Config:
        alias_generator = decapitalize_first_char


class UbwSoapService(BaseModel):
    wsdl: str  # pydantic.HttpUrl
    headers: Dict[str, str]
    credentials: UbwSoapCredentials
    alternate_endpoint: Optional[pydantic.HttpUrl]


class UbwRestEndpoint(BaseModel):
    url: Optional[str]
    headers: Dict[str, str] = {}


class UbwRestEndpoints(BaseModel):
    anlegg: UbwRestEndpoint = UbwRestEndpoint()
    begreper: UbwRestEndpoint = UbwRestEndpoint()
    begrepsverdier: UbwRestEndpoint = UbwRestEndpoint()
    project: UbwRestEndpoint = UbwRestEndpoint()
    firma: UbwRestEndpoint = UbwRestEndpoint()
    periode: UbwRestEndpoint = UbwRestEndpoint()
    bruker: UbwRestEndpoint = UbwRestEndpoint()
    arbeidsordre: UbwRestEndpoint = UbwRestEndpoint()
    gl07logs: UbwRestEndpoint = UbwRestEndpoint()
    gl07logchecks: UbwRestEndpoint = UbwRestEndpoint()
    kontoplan: UbwRestEndpoint = UbwRestEndpoint()
    konto: UbwRestEndpoint = UbwRestEndpoint()
    konteringsregler: UbwRestEndpoint = UbwRestEndpoint()
    konteringsregel: UbwRestEndpoint = UbwRestEndpoint()
    koststeder: UbwRestEndpoint = UbwRestEndpoint()
    kunder: UbwRestEndpoint = UbwRestEndpoint()
    bilagstyper: UbwRestEndpoint = UbwRestEndpoint()
    avgiftskoder: UbwRestEndpoint = UbwRestEndpoint()
    ressurser: UbwRestEndpoint = UbwRestEndpoint()
    salgsordrestatus: UbwRestEndpoint = UbwRestEndpoint()
    produkter: UbwRestEndpoint = UbwRestEndpoint()


class UbwRestBase(BaseModel):
    base_url: pydantic.HttpUrl
    headers: Dict[str, str] = {}
    use_sessions: bool = True
    endpoints: UbwRestEndpoints = UbwRestEndpoints()


class LocustWaitingTime(BaseModel):
    min_time: float
    max_time: float


class UbwClientConfig(BaseModel):
    # Base config for rest endpoints
    rest: Optional[UbwRestBase]
    import_service: Optional[UbwSoapService]
    transaction_service: Optional[UbwSoapService]
    locust_waiting_time: Optional[LocustWaitingTime]


class Begrep(BaseModel):
    attribute_id: str
    attribute_name: str
    company_id: str
    data_length: int
    data_type: str
    description: str
    dimension_position: str
    has_data_control: bool
    has_date_range_on_related_values: bool
    has_period_range_control: bool
    is_cachable: bool
    is_enabled_for_linking_contact_points: bool
    is_enabled_for_workflow: bool
    is_form_master_file: bool
    is_master_file_attribute: bool
    is_object: bool
    owner_attribute_id: str
    owner_attribute_name: str
    status: str
    type_of_maintenance: str
    type_of_unit: str
    updated_at: datetime.datetime
    updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Transaction(BaseModel):
    account: str
    amount: str
    arrival_date: Optional[datetime.date]
    compl_delay: Optional[datetime.date]
    disc_date: Optional[datetime.date]
    due_date: Optional[datetime.date]
    trans_date: Optional[datetime.date]
    voucher_date: Optional[datetime.date]
    client: str
    cur_amount: str
    currency: str
    description: str
    dim1: Optional[str]
    dim2: Optional[str]
    dim5: Optional[str]
    dim6: Optional[str]
    dim7: Optional[str]
    interface: str
    sequence_no: int
    tax_code: Optional[str]
    trans_type: str
    voucher_no: int
    voucher_type: str
    ex_ref: Optional[str]
    ext_inv_ref: Optional[str]
    ext_arch_ref_col: Optional[str]

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class Transactions(BaseModel):
    __root__: List[Transaction]


class SaveTransactionResponse(BaseModel):
    batch_id: Optional[str]
    order_no: int
    return_response: Optional[str]
    return_code: int
    return_text: Optional[str]

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class UpdatedInfo(BaseModel):
    updated_at: datetime.datetime
    updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class DurationInfo(BaseModel):
    created_at: str
    date_from: str
    date_to: str
    completed: str
    timesheet_completion_date: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class CustomerInfo(BaseModel):
    currency_code: str
    currency_type: str
    customer_id: str
    external_reference: str
    invoice_level: str
    invoice_separately: bool
    invoice_code: str
    invoice_specification: str
    invoice_status: str
    reference: str
    payment_terms_id: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class InvoiceDetails(BaseModel):
    tax_code: str
    tax_system: str
    invoice_specification: str
    payment_terms_id: str
    invoice_header_text: str
    invoice_footer_text: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Activity(BaseModel):
    ace: str
    activity_id: str
    activity_date_from: str
    activity_date_to: str
    activity_name: str
    activity_invoice_separately: str
    activity_invoice_code: str
    activity_updated_at: str
    activity_status: str
    activity_updated_by: str
    activity_wbs: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class RelatedValue(BaseModel):
    percentage: int
    date_from: datetime.datetime
    date_to: datetime.datetime
    unit_value: int
    relation_group: str
    relation_id: str
    relation_name: str
    related_value: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Contract(BaseModel):
    contract_fx: Optional[str]
    date_from_fx: Optional[datetime.datetime]
    date_to_fx: Optional[datetime.datetime]
    received_fx: Optional[str]


class PreAward(BaseModel):
    project_fx: Optional[str]
    costing_seq_fx: Optional[int]
    icosting_seq_fx: Optional[int]


class Besk(BaseModel):
    pass


class Styring(BaseModel):
    principle_fx: Optional[str]


class ContactPoints(BaseModel):
    pass


class Aofin(BaseModel):
    amount_fx: Optional[str]
    income_fx: Optional[str]
    cost_fx: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Aoegenfin(BaseModel):
    own_funding_fx: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Aoindirekte(BaseModel):
    indirect_cost_fx: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Aopartner(BaseModel):
    pass


class Bilagstype(BaseModel):
    company_id: str
    description: str
    status: str
    transaction_series: str
    transaction_type: str
    treatment_code: str
    updated_at: str
    updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class CustomFieldGroups(BaseModel):
    probesk: Besk
    prokontrakt: Contract
    propreaward: PreAward
    prostyring: Styring

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class ArbeidsordreCustomFieldGroups(BaseModel):
    aofin: Aofin
    aoegenfin: Aoegenfin
    aoindirekte: Aoindirekte
    aopartner: List[Aopartner]
    propreaward: PreAward

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Prosjekt(BaseModel):
    status: str
    wbs: str
    post_time_costs: bool
    company_id: str
    authorisation: str
    has_time_sheet_limit_control: bool
    cost_centre: str
    project_name: str
    category1: str
    category2: str
    category3: str
    category4: str
    main_project: str
    message: str
    authorise_normal_hours: bool
    authorise_overtime: bool
    project_id: str
    project_manager_id: str
    project_type: str
    workflow_state: str
    contains_work_orders: bool
    last_updated: UpdatedInfo
    project_duration: DurationInfo
    customer_information: CustomerInfo
    separate_invoice_details: Optional[InvoiceDetails]
    activities: Optional[List[Activity]]
    contact_points: List[ContactPoints]
    custom_field_groups: CustomFieldGroups
    related_values: List[RelatedValue]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Begrepsverdi(BaseModel):
    attribute_id: str
    attribute_name: str
    attribute_value: str
    company_id: str
    custom_value: float
    description: str
    owner: str
    owner_attribute_id: str
    owner_attribute_name: str
    period_from: int
    period_to: int
    status: str
    last_updated: UpdatedInfo

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Connection(BaseModel):
    currency_company_id: str
    head_office: str
    legal_acting_company_id: str
    remittance_company_id: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Accounts(BaseModel):
    exchange_gain_account: str
    exchange_loss_account: str
    cost_capitalisation: str
    income_capitalisation_account: str
    reversed_payment_account: str
    undeclared_vat_account: str
    amount_3_balance_account: str
    amount_4_balance_account: str
    balancing_attribute_account: str
    invoice_fee: str
    difference_account: str
    payment_difference_gain: str
    payment_difference_loss: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Currency(BaseModel):
    balance_attribute: str
    currency_split: bool
    currency_code: str
    triangulation_currency: str
    amount3_currency: str
    amount4_currency: str
    currency_type: str
    amount3_difference_account: str
    amount4_difference_account: str
    amount3_max_transaction_difference: int
    amount4_max_transaction_difference: int

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class VAT(BaseModel):
    undecl_vat_for_ap_notes: str
    undecl_vat_for_ar_notes: str
    undecl_vat_for_ap: str
    undecl_vat_for_ar: str
    reverse_vat_when_discount_ap: bool
    reverse_vat_when_discount_ar: bool

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AdditionalInformation(BaseModel):
    contact_person: str
    contact_position: str
    e_mail: str
    e_mail_cc: str
    gtin: str
    url: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AddressInformation(BaseModel):
    country_code: str
    place: str
    postcode: str
    province: str
    street_address: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class PhoneNumbersInformation(BaseModel):
    telephone_1: str
    telephone_2: str
    telephone_3: str
    telephone_4: str
    telephone_5: str
    telephone_6: str
    telephone_7: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class ContactPoint(BaseModel):
    additional_contact_info: AdditionalInformation
    address: AddressInformation
    contact_point_type: str
    last_updated: UpdatedInfo
    phone_numbers: PhoneNumbersInformation
    sort_order: Optional[int]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Firma(BaseModel):
    company_id: str
    company_name: str
    company_registration_number: str
    country: str
    country_code: str
    current_accounting_period: int
    dim_v2_type: str
    dim_v3_type: str
    employer_id: str
    language_code: str
    maximum_transaction_difference: int
    maximum_payment_difference: int
    municipal: str
    new_company_id: str
    overrun: int
    pay_reference: str
    number_of_periods: int
    remind_reference: str
    system_setup_code: str
    tax_office_name: str
    tax_office_reference: str
    tax_system: str
    vat_registration_number: str
    multi_company_information: Optional[Connection]
    accounting: Optional[Accounts]
    currency_information: Optional[Currency]
    vat_information: Optional[VAT]
    last_updated: UpdatedInfo
    contact_points: Optional[List[ContactPoint]]
    related_values: Optional[List[RelatedValue]]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class RoleAndCompany(BaseModel):
    role_id: str
    company_id: str
    person_id: Optional[str]
    resource_connection_updated: Optional[datetime.datetime]
    resource_connection_updated_by: Optional[str]
    role_connection_valid_from: datetime.datetime
    role_connection_valid_to: datetime.datetime
    role_connection_updated_at: datetime.datetime
    role_connection_status: str
    role_connection_updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class UserStatus(BaseModel):
    date_from: datetime.datetime
    date_to: Optional[datetime.datetime]
    status: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Security(BaseModel):
    disabled_until: Optional[datetime.datetime]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Usage(BaseModel):
    is_enabled_for_workflow_process: bool

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Bruker(BaseModel):
    user_id: str
    user_name: str
    description: str
    default_logon_company: str
    user_status: Optional[UserStatus]
    security: Optional[Security]
    language_code: Optional[str]
    usage: Optional[Usage]
    contact_points: Optional[List[ContactPoint]]
    role_and_company: Optional[List[RoleAndCompany]]

    @pydantic.root_validator(pre=True)
    def convert_role_and_company(
        cls, values: Dict[str, Any]  # noqa: N805
    ) -> Dict[str, Any]:
        if "rolesAndCompanies" in values:
            values["roleAndCompany"] = values["rolesAndCompanies"]
            del values["rolesAndCompanies"]
        return values

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class CustomerInformation(BaseModel):
    currency_code: str
    customer_id: str
    external_reference: str
    invoice_level: str
    invoice_separately: str
    invoice_code: str
    invoice_status: str
    reference: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class PartialArbeidsordre(BaseModel):
    company_id: str
    work_order_id: str
    date_from: str
    date_to: str
    period_from: str
    period_to: str
    status: str
    project_id: str
    work_order_name: str
    cost_centre: str
    project_manager_id: str
    category1: str
    last_updated: UpdatedInfo

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Arbeidsordre(BaseModel):
    company_id: str
    work_order_id: str
    date_from: str
    date_to: str
    period_from: str
    period_to: str
    status: str
    work_order_name: str
    project_id: str
    cost_centre: str
    project_manager_id: str
    category1: str
    last_updated: UpdatedInfo
    wbs: str
    category2: str
    category3: str
    category4: str
    service_order: str
    timesheet_completion_date: str
    workflow_state: str
    customer_information: CustomerInformation
    custom_field_groups: ArbeidsordreCustomFieldGroups
    related_values: List[RelatedValue]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class PeriodType(BaseModel):
    period_type: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Periode(BaseModel):
    company_id: str
    accounting_period: str
    status: str
    period_type: PeriodType

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class ServerProcessFile(BaseModel):
    file_name: str
    encoded_file: str

    @property
    def decoded_file(self) -> str:
        bytes_ = b64decode(self.encoded_file)
        #  \xef\xbb\xbf is the UTF-8 byte order mark
        encoding = (
            # Use utf-8-sig to strip BOM
            "utf-8-sig"
            if bytes_.startswith(bytes([0xEF, 0xBB, 0xBF]))
            else "latin-1"
            if self.file_name.endswith(".agm")
            else "utf-8"  # FIXME
        )

        return bytes_.decode(encoding=encoding, errors="replace")

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class ServerProcessResponse(BaseModel):
    return_code: int
    status: str

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class SoapResponse(BaseModel):
    envelope: Any  # TODO lxml.Element
    http_headers: Dict[str, str]

    class Config:
        allow_population_by_field_name = True


class Response(BaseModel):
    return_code: int
    status: Optional[str]

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class AsynchronousOutput(BaseModel):
    order_number: int
    input_table_name: Optional[str]
    namespace: Optional[str]
    response_list: Optional[List[Response]]
    soap_response: Optional[SoapResponse]

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class ServerProcessResult(BaseModel):
    error_message: Optional[str]
    file_list: List[ServerProcessFile]
    # From UBW documentation:
    #
    # ResponseList:
    #
    # An array of Response objects. See The AsyncronousOutput class above.
    # Normally, one object is returned.
    # An array of Response objects. The main properties for the Response type
    # are ReturnCode (an integer) and Status (a string). Note: If the reponse
    # list is empty, it means that everything is ok.
    response_list: List[ServerProcessResponse]
    soap_response: Optional[SoapResponse]

    class Config:
        alias_generator = to_upper_camel
        allow_population_by_field_name = True


class GL07Log(BaseModel):
    batch_id: str
    client: str
    last_update: datetime.datetime
    status: str
    voucher_no_new: Optional[int]
    voucher_no_org: int
    voucher_no_temp: Optional[int]
    voucher_type: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class GL07Logcheck(BaseModel):
    batch_id: str
    client: str
    imported: str
    orderno: int
    source: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class KontoCustomFieldGroups(BaseModel):
    pass


class Konto(BaseModel):
    account: str
    account_group: str
    account_name: str
    account_rule: int
    account_rule_name: str
    account_type: str
    company_id: str
    head_office_account: str
    period_from: int
    period_to: int
    status: str
    last_updated: UpdatedInfo
    contact_points: List[ContactPoints]
    related_values: List[RelatedValue]
    custom_field_groups: KontoCustomFieldGroups

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Kontoplan(BaseModel):
    company_id: str
    accounts: List[Konto]


class Konteringsregel(BaseModel):
    account_control: bool
    account_rule: int
    amount3_currency_type: str
    amount_flag: str
    amount_type: str
    calculate_amount3: str
    category1: str
    category1_auto_completion: int
    category1_control: bool
    category1_default_value: str
    category1_user_input: str
    category1_validation_relation: str
    category2: str
    category2_auto_completion: int
    category2_control: bool
    category2_default_value: str
    category2_user_input: str
    category2_validation_relation: str
    category3: str
    category3_auto_completion: int
    category3_control: bool
    category3_default_value: str
    category3_user_input: str
    category3_validation_relation: str
    category4: str
    category4_auto_completion: int
    category4_control: bool
    category4_default_value: str
    category4_user_input: str
    category4_validation_relation: str
    category5: str
    category5_auto_completion: int
    category5_control: bool
    category5_default_value: str
    category5_user_input: str
    category5_validation_relation: str
    category6: str
    category6_auto_completion: int
    category6_control: bool
    category6_default_value: str
    category6_user_input: str
    category6_validation_relation: str
    category7: str
    category7_auto_completion: int
    category7_control: bool
    category7_default_value: str
    category7_user_input: str
    category7_validation_relation: str
    company_id: str
    currency_auto_completion: str
    currency_default_value: str
    currency_user_input: str
    currency_validation_relation: str
    debit_credit_flag: str
    description: str
    number_text: str
    show_misc_text_on_invoice: str
    show_number_text_on_invoice: str
    show_value_text_on_invoice: str
    statistical_value_text: str
    status: str
    tax_code_auto_completion: int
    tax_code_default_value: str
    tax_code_user_input: str
    tax_code_validation_relation: str
    tax_system_auto_completion: int
    tax_system_default_value: str
    tax_system_relation_validation: str
    tax_system_user_input: str
    template_type: str
    updated_at: datetime.datetime
    updated_by: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class PercentageInfo(BaseModel):
    date_from: datetime.datetime
    date_to: datetime.datetime
    factor_vat: int
    status: str
    vat_account: str
    vat_percent: int

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class LinkedTaxCodesInfo(BaseModel):
    linked_tax_code: str
    tax_system: str
    use_base_inclusive_vat: bool

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class AvgiftskodeCustomFieldGroups(BaseModel):
    pass


class Avgiftskode(BaseModel):
    allow_amending_calculated_vat: bool
    cash_principle: bool
    company_id: str
    currency_code: str
    description: str
    rounding: int
    rounding_closest: bool
    rounding_downwards: bool
    rounding_upwards: bool
    tax_code: str
    type: str
    update_amount: bool
    used_in_accounts_payable: bool
    used_in_accounts_receivable: bool
    used_in_general_ledger: bool
    use_net_based_calculation: bool
    percentage: PercentageInfo
    last_updated: UpdatedInfo
    linked_tax_codes: Optional[List[LinkedTaxCodesInfo]]
    related_values: List[RelatedValue]
    custom_field_groups: AvgiftskodeCustomFieldGroups

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Ressurs(BaseModel):
    age: int
    birth_date: datetime.datetime
    company_id: str
    date_from: datetime.datetime
    date_to: datetime.datetime
    language_code: str
    person_id: str
    person_name: str
    personnel_type: str
    status: str
    workflow_state: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Invoice(BaseModel):
    calculate_pay_discount_on_tax: Optional[bool]
    check_credit_on_head_office: Optional[bool]
    credit_limit: Optional[float]
    currency_code: Optional[str]
    discount_code: Optional[str]
    has_fixed_currency: Optional[bool]
    has_fixed_payment_terms: Optional[bool]
    has_fixed_tax_system: Optional[bool]
    head_office: Optional[str]
    is_sundry_customer: Optional[bool]
    language_code: Optional[str]
    max_credit_days: Optional[int]
    message: Optional[str]
    payment_plan_template_code: Optional[str]
    payment_terms_id: Optional[str]
    supplier_id: Optional[str]
    tax_system: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Payment(BaseModel):
    bank_account: Optional[str]
    bank_clearing_code: Optional[str]
    debt_collection_code: Optional[str]
    expiry_date: Optional[str]
    iban: Optional[str]
    other_account: Optional[str]
    payment_method: Optional[str]
    pay_recipient: Optional[str]
    postal_account: Optional[str]
    status: Optional[str]
    swift: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class KundeCustomFieldGroups(BaseModel):
    pass


class Kunde(BaseModel):
    alias_name: Optional[str]
    company_id: Optional[str]
    company_registration_number: Optional[str]
    country_code: Optional[str]
    customer_group_id: Optional[str]
    customer_id: Optional[str]
    customer_name: Optional[str]
    external_reference: Optional[str]
    note: Optional[str]
    vat_registration_number: Optional[str]
    workflow_state: Optional[str]
    invoice: Optional[Invoice]
    payment: Optional[Payment]
    contact_points: Union[List[ContactPoint], ContactPoint, None]
    custom_field_groups: Optional[KundeCustomFieldGroups]
    related_values: Union[List[RelatedValue], RelatedValue, None]
    last_updated: Optional[UpdatedInfo]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class StatusOrder(str, enum.Enum):
    MOTTATT = "N"
    FAKTURERT = "F"
    PARKERT = "P"
    SPERRET = "C"
    DELFAKTURERT = "D"
    TERMINERT = "T"
    FORSYSTEM = "E"

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Salgsordrestatus(BaseModel):
    amount: Optional[float]
    apar_id: Optional[str]
    batch_id: Optional[str]
    client: Optional[str]
    comp_reg_no: Optional[str]
    due_date: Optional[str]
    ext_inv_ref: Optional[str]
    ext_order_id: Optional[str]
    import_date: Optional[str]
    info: Optional[str]
    invoice_date: Optional[str]
    number_type: Optional[str]
    order_id: Optional[int]
    orderno: Optional[int]
    pay_date: Optional[str]
    rest_amount: Optional[float]
    status_descr: Optional[str]
    status_order: Union[StatusOrder, str, None]
    status_pay: Optional[str]
    text3: Optional[str]
    text4: Optional[str]
    voucher_no: Optional[int]
    voucher_type: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Produkt(BaseModel):
    product_id: Optional[str]
    alias: Optional[str]
    article_id: Optional[int]
    company_id: Optional[str]
    for_purchase: Optional[bool]
    for_sale: Optional[bool]
    gtin: Optional[str]
    include_in_order_discount: Optional[bool]
    is_external: Optional[bool]
    is_searchable: Optional[bool]
    is_unique_product: Optional[bool]
    main_supplier_id: Optional[str]
    product_description: Optional[str]
    product_group: Optional[str]
    provide_search_information: Optional[bool]
    status: Optional[str]
    unit: Optional[str]
    use_amount_not_units: Optional[bool]

    # other fields not needed atm:
    # purchase_information
    # sales_pricing
    # purchase_pricing
    # inventory_information
    # last_updated
    # related_values
    # contact_points
    # custom_field_groups

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True
