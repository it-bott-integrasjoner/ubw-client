import pydantic
import pytest

from ubw_client.models import (
    Arbeidsordre,
    Begrep,
    Begrepsverdi,
    Bilagstype,
    Bruker,
    GL07Log,
    Invoice,
    Konto,
    Kunde,
    Payment,
    Periode,
    Prosjekt,
    RelatedValue,
    Ressurs,
    UbwRestEndpoints,
    Produkt,
)


def test_begrep(begrep_data):
    r = [Begrep.from_dict(x) for x in begrep_data]
    assert len(r) == 6


def test_begrepsverdier(begrepsverdier_data):
    r = [Begrepsverdi.from_dict(x) for x in begrepsverdier_data]
    assert len(r) == 6


def test_prosjekt(project_data):
    r = Prosjekt(**project_data)
    assert r.project_name == project_data["projectName"]


def test_bruker(bruker_data):
    bruker = Bruker.from_dict(bruker_data)
    assert hasattr(bruker.user_status, "status")
    assert hasattr(bruker.security, "disabled_until")
    assert hasattr(bruker.usage, "is_enabled_for_workflow_process")
    assert bruker.role_and_company
    assert bruker.contact_points


def test_bruker_singular(bruker_singular_data):
    bruker = Bruker.from_dict(bruker_singular_data)
    assert hasattr(bruker.user_status, "status")
    assert hasattr(bruker.security, "disabled_until")
    assert hasattr(bruker.usage, "is_enabled_for_workflow_process")
    assert bruker.role_and_company
    assert bruker.contact_points


def test_arbeidsordre(arbeidsordre_data):
    r = Arbeidsordre.from_dict(arbeidsordre_data[0])
    assert r.company_id == arbeidsordre_data[0]["companyId"]


def test_periode(periode_data):
    r = Periode.from_dict(periode_data[0])
    assert r.company_id == periode_data[0]["companyId"]
    assert r.period_type.period_type == periode_data[0]["periodType"]["periodType"]


def test_gl07logs(gl07logs_data):
    gl07logs = [GL07Log.from_dict(log) for log in gl07logs_data]
    for i, log in enumerate(gl07logs):
        assert log.batch_id == gl07logs_data[i]["batchId"]
        assert log.client == gl07logs_data[i]["client"]
        assert log.last_update
        assert log.status == gl07logs_data[i]["status"]
        assert log.voucher_no_new == gl07logs_data[i]["voucherNoNew"]
        assert log.voucher_no_org == gl07logs_data[i]["voucherNoOrg"]
        assert log.voucher_no_temp == gl07logs_data[i]["voucherNoTemp"]
        assert log.voucher_type == gl07logs_data[i]["voucherType"]


def test_ubw_rest_endpoints():
    try:
        UbwRestEndpoints.from_dict({})
    except pydantic.ValidationError as e:
        pytest.fail(f"Unexpected validation error: {e}")


def test_konto(konto_data):
    model = Konto(**konto_data[0])
    assert model.company_id == konto_data[0]["companyId"]
    assert model.account == konto_data[0]["account"]
    assert model.account_group == konto_data[0]["accountGroup"]
    assert model.account_name == konto_data[0]["accountName"]
    assert model.account_rule == konto_data[0]["accountRule"]
    assert model.account_rule_name == konto_data[0]["accountRuleName"]
    assert model.account_type == konto_data[0]["accountType"]
    assert model.company_id == konto_data[0]["companyId"]
    assert model.head_office_account == konto_data[0]["headOfficeAccount"]
    assert model.period_from == konto_data[0]["periodFrom"]
    assert model.period_to == konto_data[0]["periodTo"]
    assert model.status == konto_data[0]["status"]


def test_bilagstype(bilagstype_data):
    model = Bilagstype(**bilagstype_data[0])
    assert model.company_id == bilagstype_data[0]["companyId"]
    assert model.description == bilagstype_data[0]["description"]
    assert model.status == bilagstype_data[0]["status"]
    assert model.transaction_series == bilagstype_data[0]["transactionSeries"]
    assert model.transaction_type == bilagstype_data[0]["transactionType"]
    assert model.treatment_code == bilagstype_data[0]["treatmentCode"]
    assert model.updated_at == bilagstype_data[0]["updatedAt"]
    assert model.updated_by == bilagstype_data[0]["updatedBy"]


def test_ressurs(ressurs_data):
    model = Ressurs(**ressurs_data[0])
    assert model.age == ressurs_data[0]["age"]
    assert model.company_id == ressurs_data[0]["companyId"]
    assert model.language_code == ressurs_data[0]["languageCode"]
    assert model.person_id == ressurs_data[0]["personId"]
    assert model.person_name == ressurs_data[0]["personName"]
    assert model.personnel_type == ressurs_data[0]["personnelType"]
    assert model.status == ressurs_data[0]["status"]
    assert model.workflow_state == ressurs_data[0]["workflowState"]


def test_kunde(kunde_old_data):
    model = Kunde(**kunde_old_data)
    assert model.customer_name == "string"
    assert isinstance(model.invoice, Invoice)
    assert isinstance(model.related_values, list)
    assert isinstance(model.related_values[0], RelatedValue)
    assert isinstance(model.payment, Payment)
    assert model.dict().get("customerName") == "string"


def test_produkt(produkt_data):
    model = Produkt(**produkt_data)
    assert model.article_id == 14726
    assert model.product_id == "UN-10332003"
    assert model.for_sale == False
    assert model.for_purchase == True
    assert model.status == "N"
    assert model.unit == "EA"
