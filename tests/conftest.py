import json
import os

import pytest
import yaml
from collections import defaultdict
from xml.etree import ElementTree
import re
import requests_mock
from typing import Dict, Union

from ubw_client.client import get_rest_client, get_soap_client


@pytest.fixture
def base_url():
    return "https://example.com/"


@pytest.fixture
def config(base_url):
    return {
        "rest": {
            "base_url": base_url,
            "headers": {},
            "endpoints": {
                "begreper": {"headers": {}},
                "begrepsverdier": {"headers": {}},
                "project": {"headers": {}},
                "firma": {"headers": {}},
                "periode": {"headers": {}},
                "arbeidsordre": {"headers": {}},
                "bruker": {"headers": {}},
                "gl07logs": {"headers": {}},
                "gl07logchecks": {"headers": {}},
                "konto": {"headers": {}},
                "kontoplan": {"headers": {}},
                "konteringsregel": {"headers": {}},
                "konteringsregler": {"headers": {}},
                "bilagstyper": {"headers": {}},
                "ressurs": {"headers": {}},
                "salgsordrestatus": {"headers": {}},
            },
        },
        "import_service": {
            "wsdl": "wsdl/importservice.wsdl",
            "headers": {},
            "credentials": {
                "username": "fake_test_user",
                "client": "72",
                "password": "fake_test_password",
            },
            "alternate_endpoint": "https://example.com/soap/importtjeneste",
        },
        "transaction_service": {
            "wsdl": "wsdl/batchinputservice.wsdl",
            "headers": {},
            "credentials": {
                "username": "fake_test_user",
                "client": "72",
                "password": "fake_test_password",
            },
            "alternate_endpoint": "https://example.com/soap/batchinputservice",
        },
    }


@pytest.fixture
def config_no_overrides(base_url):
    return {
        "rest": {
            "base_url": base_url,
            "headers": {},
            "endpoints": {
                "begreper": {},
                "begrepsverdier": {},
                "project": {},
                "firma": {},
                "periode": {},
                "bruker": {},
                "arbeidsordre": {},
                "gl07logs": {},
                "gl07logchecks": {},
                "konto": {},
                "kontoplan": {},
                "konteringsregel": {},
                "konteringsregler": {},
                "bilagstyper": {},
                "ressurs": {},
                "salgsordrestatus": {},
            },
        },
        "import_service": None,
        "transaction_service": None,
    }


# Used in integration tests, default: :disabled
@pytest.fixture
def service_config():
    return yaml.load(load_file("config.yaml"), Loader=yaml.FullLoader)


@pytest.fixture
def rest_client(config):
    rest_client = get_rest_client(**config)
    return rest_client


@pytest.fixture
def soap_client(config):
    soap_client = get_soap_client(**config)
    return soap_client


def load_file(file):
    with open(file) as f:
        return f.read()


def load_json_file(name):
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(here, "fixtures", name)) as f:
        data = json.load(f)
    return data


def load_xml(file, strip_namespace=True):
    def etree_to_dict(t: ElementTree.Element):
        if strip_namespace:
            t.tag = re.sub(r"^{[^}]+}", "", t.tag)
        d: Dict[str, Union[Dict[str, str], str, None]] = {
            t.tag: {} if t.attrib else None
        }
        children = list(t)
        if children:
            dd = defaultdict(list)
            for dc in map(etree_to_dict, children):
                for k, v in dc.items():
                    dd[k].append(v)
            d = {t.tag: {k: v[0] if len(v) == 1 else v for k, v in dd.items()}}
        if t.attrib:
            d[t.tag].update(("@" + k, v) for k, v in t.attrib.items())  # type: ignore[union-attr]
        if t.text:
            text = t.text.strip()
            if children or t.attrib:
                if text and d[t.tag] is not None:
                    d[t.tag]["#text"] = text  # type: ignore[index]
            else:
                d[t.tag] = text
        return d

    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

    return etree_to_dict(
        ElementTree.XML(load_file(os.path.join(here, "fixtures", file)))
    )


@pytest.fixture
def anlegg_many_data():
    return load_json_file("anlegg.json")


@pytest.fixture
def anlegg_single_data():
    return load_json_file("anlegg_10000011.json")


@pytest.fixture
def begrep_data():
    return load_json_file("begreper.json")


@pytest.fixture
def begrepsverdier_data():
    return load_json_file("begrepsverdier.json")


@pytest.fixture
def bilagstype_data():
    return load_json_file("bilagstype.json")


@pytest.fixture
def bilagstyper_data():
    return load_json_file("bilagstyper.json")


@pytest.fixture
def project_data():
    return load_json_file("project.json")


@pytest.fixture
def firma_data():
    return load_json_file("firma.json")


@pytest.fixture
def arbeidsordre_data():
    return load_json_file("arbeidsordre.json")


@pytest.fixture
def periode_data():
    return load_json_file("periode.json")


@pytest.fixture
def bruker_data():
    return load_json_file("bruker.json")


@pytest.fixture
def bruker_singular_data():
    return load_json_file("bruker_singular.json")


@pytest.fixture
def gl07logs_data():
    return load_json_file("gl07logs.json")


@pytest.fixture
def gl07logchecks_data():
    return load_json_file("gl07logchecks.json")


@pytest.fixture
def konto_data():
    return load_json_file("konto.json")


@pytest.fixture
def konteringsregler_data():
    return load_json_file("konteringsregler.json")


@pytest.fixture
def koststeder_data():
    return load_json_file("koststeder.json")


@pytest.fixture
def kunde_data():
    return load_json_file("kunde.json")


@pytest.fixture
def kunde_old_data():
    return load_json_file("kunde_old.json")


@pytest.fixture
def avgiftskoder_data():
    return load_json_file("avgiftskoder.json")


@pytest.fixture
def salgsordrestatus_data():
    return load_json_file("salgsordrestatus.json")


@pytest.fixture
def salgsordre():
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    return load_file(os.path.join(here, "fixtures", "salgsordre.xml"))


@pytest.fixture
def salgsordre_to_unit4_content():
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    return load_file(os.path.join(here, "fixtures", "salgsordre_to_unit4_content.xml"))


@pytest.fixture
def salgsordre_response():
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    return load_file(os.path.join(here, "fixtures", "salgsordre_response.xml"))


@pytest.fixture
def transaction_report_data():
    return load_xml("transaction_report.xml")


@pytest.fixture
def transaction_report_data_faulty():
    return load_xml("transaction_report_faulty.xml")


@pytest.fixture
def save_transactions_response():
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    return load_file(os.path.join(here, "fixtures", "save_transactions_response.xml"))


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)


@pytest.fixture
def ressurs_data():
    return load_json_file("ressurs.json")


@pytest.fixture
def ressurser_data():
    return load_json_file("ressurser.json")


@pytest.fixture
def produkt_data():
    return load_json_file("produkt.json")


@pytest.fixture
def mock_api():
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture
def ascii_control_characters():
    return "".join([chr(x) for x in range(0x20)])
