import datetime
import itertools
import logging.config
from types import ModuleType
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Literal,
    Optional,
    overload,
    Type,
    Tuple,
    Union,
)
from urllib.parse import urljoin, urlparse
import lxml.etree  # nosec

import requests
import zeep
import zeep.exceptions
from zeep import Transport
from zeep.helpers import serialize_object as zeep_serialize
from zeep.plugins import HistoryPlugin

from . import models

ComplexType = Any
JsonType = Any

logger = logging.getLogger(__name__)


class ClientError(Exception):
    pass


class IncorrectPathError(Exception):
    pass


class NotFoundException(Exception):
    """An exception similar in meaning to a HTTP 404, but raised by the client
    on its own initiative, rather than received as an HTTP response.
    """

    pass


def none_or_error(e: requests.RequestException) -> None:
    """Accepts: an error after a request. Returns:
    None if 404, raises the error itself in any other case.
    """
    if e.response is None:
        raise AssertionError("Exception has no response")
    elif e.response.status_code == 404:
        try:
            content = e.response.json()
        except requests.JSONDecodeError:
            # Correctly configured endpoint should always return valid JSON
            raise IncorrectPathError
        else:
            error_code = content.get("code")
            if error_code == 1040:
                return None
            raise IncorrectPathError

    e.response.raise_for_status()
    raise IncorrectPathError


def _list_or_obj(
    cls: Type[models.T], resp: Union[Dict[str, Any], List[Dict[str, Any]], None]
) -> Union[List[models.T], models.T, None]:
    if not resp:
        return None

    if isinstance(resp, list):
        return [cls(**prop) for prop in resp]
    else:
        return cls(**resp)


class Endpoints:
    def __init__(self, rest: models.UbwRestBase):
        self.rest = rest

    def _prepend_base_url(self, path: str, *idents: Optional[str]) -> str:
        idents_ = [str(i) for i in idents if i]
        return urljoin(
            self.rest.base_url.rstrip("/") + "/",
            path.rstrip("/") + "/" + "/".join(idents_),
        )

    def get_anlegg(self, firma: str, anlegg: Optional[str] = None) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.anlegg.url or "anlegg",
            firma,
            anlegg,
        )

    def get_begreper(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.begreper.url or "begreper", company_id
        )

    def get_begrepsverdier(
        self, company_id: str, begrep_id: Optional[str] = None
    ) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.begrepsverdier.url or "begrepsverdier",
            company_id,
            begrep_id,
        )

    def get_prosjekter(self, company_id: str, project_id: Optional[str] = None) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.project.url or "prosjekter", company_id, project_id
        )

    def get_firma(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.firma.url or "firma", company_id
        )

    def get_arbeidsordre(self, company_id: str, ordre_id: Optional[str] = None) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.arbeidsordre.url or "arbeidsordre", company_id, ordre_id
        )

    def get_perioder(self, company_id: str, period: Optional[str] = None) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.periode.url or "regnskapsperioder", company_id, period
        )

    def get_bruker(self, company_id: str, bruker_id: Optional[str] = None) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.bruker.url or "brukere", company_id, bruker_id
        )

    def get_gl07logs(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.gl07logs.url or "objekter/gl07logs", company_id
        )

    def get_gl07logchecks(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.gl07logchecks.url or "gl07logchecks", company_id
        )

    def get_kontoplan(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.kontoplan.url or "kontoplan/v1", company_id
        )

    def get_konto(self, company_id: str, konto: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.konto.url or "kontoplan/v1", company_id, konto
        )

    def get_konteringsregler(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.konteringsregler.url or "konteringsregler/v1",
            company_id,
        )

    def get_konteringsregel(self, company_id: str, konto: Union[int, str]) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.konteringsregel.url or "konteringsregler/v1",
            company_id,
            str(konto),
        )

    def get_koststed(self, company_id: str, koststed: Optional[str]) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.koststeder.url or "koststeder/v1",
            company_id,
            koststed,
        )

    def get_kunder(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.kunder.url or "kunder/v1",
            company_id,
        )

    def get_kunde(self, company_id: str, kunde_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.kunder.url or "kunder/v1", company_id, kunde_id
        )

    def get_bilagstyper(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.bilagstyper.url or "bilagstyper/v1", company_id
        )

    def get_bilagstype(self, company_id: str, transaction_type: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.bilagstyper.url or "bilagstyper/v1",
            company_id,
            transaction_type,
        )

    def get_avgiftskode(
        self, company_id: str, avgiftskode: Optional[str] = None
    ) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.avgiftskoder.url or "avgiftskoder/v1",
            company_id,
            avgiftskode,
        )

    def get_ressurser(self, company_id: str, ressurs_id: Optional[str] = None) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.ressurser.url or "ressurser/v1", company_id, ressurs_id
        )

    def get_salgsordrestatus(self, company_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.salgsordrestatus.url or "salgsordrestatus/v1",
            company_id,
        )

    def get_produkt(self, company_id: str, produkt_id: str) -> str:
        return self._prepend_base_url(
            self.rest.endpoints.produkter.url or "produkter/v1",
            company_id,
            produkt_id,
        )


def _get_list(response_dict: Dict[str, Dict[str, Any]], a: str, b: str) -> List[Any]:
    """
    Extract list from response dict.

    >>> _get_list({"a": {"b": ["c", "d", "e"]}}, "a", "b")
    ['c', 'd', 'e']
    >>> _get_list({"a": {"b": "c"}}, "a", "b")
    ['c']
    >>> _get_list({"a": {"b": "c"}}, "a", "d")
    []
    """

    x = (response_dict.get(a) or {}).get(b)
    return x if isinstance(x, list) else [x] if x else []


def _create_server_process_result(
    transaction_result_response: Any,
) -> models.ServerProcessResult:
    """Create a ServerProcessResult from dict or zeep object"""
    response_dict = (
        transaction_result_response
        if isinstance(transaction_result_response, dict)
        else zeep_serialize(
            transaction_result_response
        )  # type: ignore[no-untyped-call]
    )

    file_list = _get_list(response_dict, "B64FileList", "B64File")
    response_list = _get_list(response_dict, "ResponseList", "Response")
    data = {
        "error_message": response_dict.get("ErrorMessage"),
        "file_list": file_list,
        "response_list": response_list,
    }
    spr = models.ServerProcessResult.from_dict(data)

    _validate_existing_log_file(spr)

    return spr


def _validate_existing_log_file(spr: models.ServerProcessResult) -> None:
    if not spr.file_list:
        return

    found_log_file = any([".log" in x.file_name for x in spr.file_list])
    if not found_log_file:
        raise ValueError("Invalid ServerProcessResult, missing log files in resp")


def _create_asynchronous_output(
    asynchronous_output_response: Any,
) -> models.AsynchronousOutput:
    """Create an AsynchronousOutput from dict or zeep object"""
    response_dict = (
        asynchronous_output_response
        if isinstance(asynchronous_output_response, dict)
        else zeep_serialize(
            asynchronous_output_response
        )  # type: ignore[no-untyped-call]
    )

    data = {
        "order_number": response_dict.get("OrderNumber"),
        "input_table_name": response_dict.get("InputTableName"),
        "namespace": response_dict.get("Namespace"),
        "response_list": _get_list(response_dict, "ResponseList", "Response"),
    }

    return models.AsynchronousOutput(**data)


# Allow TAB, CR, LF
_CONTROL_CHARACTER_TRANS = str.maketrans(
    dict(zip(set(range(0x20)) - {9, 10, 13}, itertools.repeat("\ufffd")))
)


def _replace_control_characters(content: Union[str, Any]) -> Union[str, Any]:
    if isinstance(content, str):
        return content.translate(_CONTROL_CHARACTER_TRANS)
    else:
        return content


def _map_transaction(transaction: models.Transaction) -> Dict[str, Any]:
    date_fields = [
        "arrival_date",
        "compl_delay",
        "disc_date",
        "due_date",
        "trans_date",
        "voucher_date",
    ]
    optional_fields = [
        "dim1",
        "dim2",
        "dim3",
        "dim4",
        "dim5",
        "dim6",
        "dim7",
        "tax_code",
        "ext_arch_ref_col",
    ]
    mapped: Dict[str, Any] = transaction.dict()

    for optional_field in optional_fields:
        mapped.setdefault(
            models.Transaction.Config.alias_generator(optional_field),
            zeep.xsd.SkipValue,
        )

    for date_field in date_fields:
        mapped.setdefault(
            models.Transaction.Config.alias_generator(date_field),
            datetime.date(year=1900, month=1, day=1),
        )

    return {k: _replace_control_characters(v) for k, v in mapped.items()}


class UBWSoapClient:
    def __init__(self, config: models.UbwClientConfig) -> None:
        self.config = config
        if config.rest is None:
            raise ValueError("Missing rest config")
        self.urls = Endpoints(config.rest)
        self.history = HistoryPlugin()  # type: ignore[no-untyped-call]

        self.import_client, self.import_service = self._build_soap_service(
            config=config.import_service, transport=Transport
        )

        self.transaction_client, self.transaction_service = self._build_soap_service(
            config=config.transaction_service, transport=Transport
        )

    def _build_soap_service(
        self,
        config: Optional[models.UbwSoapService],
        transport: Callable[..., Any],
    ) -> Tuple[Optional[zeep.client.Client], Optional[zeep.proxy.ServiceProxy]]:
        if not config:
            return None, None

        session = requests.Session()
        session.headers.update(config.headers)

        client = zeep.Client(  # type: ignore[no-untyped-call]
            wsdl=config.wsdl,
            transport=transport(session=session),
            plugins=[self.history],
        )

        if config.alternate_endpoint:
            logger.debug(
                "Using alternative configured endpoint: %s", config.alternate_endpoint
            )
            return (
                client,
                client.create_service(  # type: ignore[no-untyped-call]
                    # TODO: Is this correct?
                    list(client.wsdl.bindings)[0],
                    config.alternate_endpoint,
                ),
            )
        else:
            return client, client.service

    def check_if_can_delete_batch_input(self, batch_id: str, interface_id: str) -> bool:
        if (
            self.transaction_client is None
            or self.transaction_service is None
            or self.config.transaction_service is None
        ):
            raise ClientError("Transaction API is not configured")

        logger.info(
            "Checking if we can delete batch input for batchid,interface: %s, %s",
            batch_id,
            interface_id,
        )
        try:
            req = self.transaction_service.CanDeleteBatchInput(
                batchId=batch_id,
                interfaceId=interface_id,
                credentials=dict(self.config.transaction_service.credentials),
            )
        except zeep.exceptions.Fault as e:
            logger.warning("Ubw-client error: %s", e.code)
            raise ClientError(e.code) from e

        logger.info("Received the following from ubw api: %s", req)

        # Response data contains string with either
        # "400, NoRowsToDelete" or "200, CanDeleteRows"
        return len(req) > 3 and req.startswith("200")

    def delete_batch_input(self, batch_id: str, interface_id: str) -> bool:
        if (
            self.transaction_client is None
            or self.transaction_service is None
            or self.config.transaction_service is None
        ):
            raise ClientError("Transaction API is not configured")

        logger.info(
            "Requesting to delete batch input for batchid,interface: %s, %s",
            batch_id,
            interface_id,
        )
        try:
            req = self.transaction_service.DeleteBatchInput(
                batchId=batch_id,
                interfaceId=interface_id,
                credentials=dict(self.config.transaction_service.credentials),
            )
        except zeep.exceptions.Fault as e:
            logger.warning("Ubw-client error: %s", e.code)
            raise ClientError(e.code) from e

        logger.info("Received the following from ubw api: %s", req)

        # Response data contains either
        # "200 Operation succeeded" or "400 Operation failed"
        return len(req) > 3 and req.startswith("200")

    def save_transactions(
        self,
        transactions: models.Transactions,
        process_parameters: Tuple[str, str],
        report_variant: int,
        period: int,
        batch_id: str,
    ) -> models.SaveTransactionResponse:
        if (
            self.transaction_client is None
            or self.transaction_service is None
            or self.config.transaction_service is None
        ):
            raise ClientError("Transaction API is not configured")

        batch_inputs = []
        type_factory = self.transaction_client.type_factory(
            "ns0"
        )  # type: ignore[no-untyped-call]
        for t in transactions.__root__:
            batch_inputs.append(
                type_factory.BatchInputDTO(
                    **_map_transaction(t),
                    AllocationKey=zeep.xsd.SkipValue,
                    ArriveId=zeep.xsd.SkipValue,
                    BaseAmount=zeep.xsd.SkipValue,
                    BaseCurr=zeep.xsd.SkipValue,
                    Collection=zeep.xsd.SkipValue,
                    DcFlag=zeep.xsd.SkipValue,
                    Discount=zeep.xsd.SkipValue,
                    ErrorFlag=zeep.xsd.SkipValue,
                    ExchRate=zeep.xsd.SkipValue,
                    ExchRate2=zeep.xsd.SkipValue,
                    ExchRate3=zeep.xsd.SkipValue,
                    Number1=zeep.xsd.SkipValue,
                    OrigReference=zeep.xsd.SkipValue,
                    PayFlag=zeep.xsd.SkipValue,
                    PayPlanId=zeep.xsd.SkipValue,
                    PeriodNo=zeep.xsd.SkipValue,
                    RemittId=zeep.xsd.SkipValue,
                    SequenceRef=zeep.xsd.SkipValue,
                    TaxId=zeep.xsd.SkipValue,
                    Value1=zeep.xsd.SkipValue,
                    Value2=zeep.xsd.SkipValue,
                    Value3=zeep.xsd.SkipValue,
                    VoucherRef=zeep.xsd.SkipValue,
                )
            )

        batch_input_process_type = type_factory.BatchInputProcessType(
            ParameterList=type_factory.ArrayOfProcessParameters(
                [
                    type_factory.ProcessParameters(
                        Name=process_parameters[0], Value=process_parameters[1]
                    )
                ]
            ),
            ReportVariant=report_variant,
        )

        batch_input_array = {"BatchInputDTO": batch_inputs}
        try:
            r = self.transaction_service.SaveTransactions(
                batchInput=batch_input_array,
                parameters=batch_input_process_type,
                period=period,
                batchId=batch_id,
                credentials=dict(self.config.transaction_service.credentials),
            )
        except zeep.exceptions.Fault as e:
            raise ClientError(e.code) from e

        return models.SaveTransactionResponse(**dict((x, getattr(r, x)) for x in r))

    def execute_server_process_asynchronously(
        self,
        variant: int,
        xml: str,
        menu_id: Union[str, zeep.xsd.const._StaticIdentity] = zeep.xsd.SkipValue,
        process_parameters: Union[
            Tuple[str, str], zeep.xsd.const._StaticIdentity
        ] = zeep.xsd.SkipValue,
        server_process_id: Union[
            str, zeep.xsd.const._StaticIdentity
        ] = zeep.xsd.SkipValue,
        stylesheet: Union[str, zeep.xsd.const._StaticIdentity] = zeep.xsd.SkipValue,
        pipeline_associated_name: Union[
            str, zeep.xsd.const._StaticIdentity
        ] = zeep.xsd.SkipValue,
    ) -> models.AsynchronousOutput:
        """
        Method for using the ExecuteServerProcessAsynchronously of the Import service
        """
        if (
            self.import_client is None
            or self.import_service is None
            or self.config.import_service is None
        ):
            raise ClientError("Import API is not configured")

        type_factory = self.import_client.type_factory(
            "ns0"
        )  # type: ignore[no-untyped-call]
        parameter_list: Union[Tuple[Any], zeep.xsd.const._StaticIdentity]
        if process_parameters is not zeep.xsd.SkipValue:
            parameter_list = (
                type_factory.ArrayOfParameter(
                    [
                        type_factory.Parameter(
                            Name=process_parameters[0],  # type: ignore[index]
                            Value=process_parameters[1],  # type: ignore[index]
                        )
                    ]
                ),
            )
        else:
            parameter_list = zeep.xsd.SkipValue

        xml_text = lxml.etree.CDATA(xml)
        server_process_input = type_factory.ServerProcessInput(
            ServerProcessId=server_process_id,
            MenuId=menu_id,
            Variant=variant,
            Xml=xml_text,
            Stylesheet=stylesheet,
            ParameterList=parameter_list,
            PipelineAssociatedName=pipeline_associated_name,
        )
        try:
            asynchronous_output = (
                self.import_service.ExecuteServerProcessAsynchronously(
                    input=server_process_input,
                    credentials=dict(self.config.import_service.credentials),
                )
            )
            output = _create_asynchronous_output(asynchronous_output)
            if self.history.last_received is None:
                raise ValueError("Missing last_received")
            output.soap_response = models.SoapResponse(**self.history.last_received)
            return output

        except zeep.exceptions.Fault as e:
            raise ClientError() from e

    def get_transaction_report(
        self, server_process_id: str, order_nr: int
    ) -> models.ServerProcessResult:
        if self.import_service is None or self.config.import_service is None:
            raise ClientError("Import API is not configured")

        result_query = self.create_result_query(server_process_id, order_nr)
        try:
            transaction_result_resp = self.import_service.GetResult(
                input=result_query,
                credentials=dict(self.config.import_service.credentials),
            )

            report = _create_server_process_result(transaction_result_resp)
            if self.history.last_received is None:
                raise ValueError("Missing last_received")
            report.soap_response = models.SoapResponse(**self.history.last_received)
            return report

        except zeep.exceptions.Fault as e:
            raise ClientError() from e

    def get_transaction_status(self, server_process_id: str, order_nr: int) -> str:
        if (
            self.import_client is None
            or self.import_service is None
            or self.config.import_service is None
        ):
            raise ClientError("Import API is not configured")

        result_query = self.create_result_query(server_process_id, order_nr)

        try:
            transaction_status_resp = self.import_service.PollStatus(
                input=result_query,
                credentials=dict(self.config.import_service.credentials),
            )

        except zeep.exceptions.Fault as e:
            raise ClientError() from e

        transaction_status_result: str = transaction_status_resp.Result
        return transaction_status_result.strip()

    def create_result_query(self, server_process_id: str, order_nr: int) -> ComplexType:
        if self.import_client is None:
            raise ClientError("Import API is not configured")

        type_factory = self.import_client.type_factory(
            "ns0"
        )  # type: ignore[no-untyped-call]
        result_query = type_factory.ResultQuery(
            ServerProcessId=server_process_id, OrderNumber=order_nr
        )
        return result_query


class UBWRestClient:
    def __init__(self, config: models.UbwClientConfig) -> None:
        self.config = config
        if config.rest is None:
            raise ValueError("Missing rest config")
        self.urls = Endpoints(config.rest)

        self.rest_session: Union[ModuleType, requests.Session]
        if config.rest and config.rest.use_sessions:
            self.rest_session = requests.Session()
        else:
            self.rest_session = requests

    @overload
    def _get_with_try(
        self,
        url: str,
        params: Optional[Dict[str, Any]] = None,
        headers: Optional[Dict[str, Any]] = None,
        get_first: Literal[True] = True,
    ) -> Union[JsonType, None]:
        ...

    @overload
    def _get_with_try(
        self,
        url: str,
        params: Optional[Dict[str, Any]] = None,
        headers: Optional[Dict[str, Any]] = None,
        get_first: Literal[False] = False,
    ) -> Union[JsonType, requests.Response, None]:
        ...

    def _get_with_try(
        self,
        url: str,
        params: Optional[Dict[str, Any]] = None,
        headers: Optional[Dict[str, Any]] = None,
        get_first: Optional[bool] = False,
    ) -> Union[JsonType, requests.Response, None]:
        try:
            r = self.get(url, headers=headers, params=params)
        except requests.HTTPError as e:
            return none_or_error(e)  # type: ignore[func-returns-value]

        if not get_first:
            return r

        if not r:
            return None

        return r[0]  # type: ignore[index]

    def _build_headers(self, headers: Dict[str, Any]) -> Dict[str, Any]:
        request_headers = {}
        if self.config.rest is not None:
            for h in self.config.rest.headers:
                request_headers[h] = self.config.rest.headers[h]
        for h in headers or ():
            request_headers[h] = headers[h]
        return request_headers

    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Optional[Dict[str, Any]] = None,
        return_response: Optional[bool] = False,
        **kwargs: Any,
    ) -> Union[JsonType, requests.Response]:
        logger.debug(
            "Calling %s %s with params=%r", method_name, urlparse(url).path, params
        )
        r = self.rest_session.request(
            method_name,
            url,
            headers=(
                self._build_headers({})
                if headers is None
                else self._build_headers(headers)
            ),
            params=params if params is not None else {},
            **kwargs,
        )
        logger.debug("GOT HTTP %d: %r", r.status_code, r.content)

        if return_response:
            return r
        else:
            r.raise_for_status()
            return r.json()

    def post(self, url: str, **kwargs: Any) -> Union[JsonType, requests.Response]:
        return self.call("POST", url, **kwargs)

    def get(self, url: str, **kwargs: Any) -> Union[JsonType, requests.Response]:
        return self.call("GET", url, **kwargs)

    def put(self, url: str, **kwargs: Any) -> Union[JsonType, requests.Response]:
        return self.call("PUT", url, **kwargs)

    def patch(self, url: str, **kwargs: Any) -> Union[JsonType, requests.Response]:
        return self.call("PATCH", url, **kwargs)

    def delete(self, url: str, **kwargs: Any) -> Union[JsonType, requests.Response]:
        return self.call("DELETE", url, **kwargs)

    def get_anlegg(
        self,
        company_id: str,
        anlegg: Optional[str] = None,
        filters: Optional[Dict[str, Union[str, int]]] = None,
    ) -> Union[List[models.Begrepsverdi], models.Begrepsverdi, None]:
        """
        Get all or single anlegg of company with company_id

        Accepts a dict of filters with the following keys:

        "attributeId", "attributeName", "attributeValue", "companyId",
        "customValue", "owner", "ownerAttributeId", "ownerAttributeName",
        "periodFrom" "periodTo", "status"

        with values as int or str
        """
        url = self.urls.get_anlegg(company_id, anlegg)
        headers = (
            self.config.rest.endpoints.anlegg.headers if self.config.rest else dict()
        )
        if anlegg and filters:
            raise ValueError("Filters are not allowed when getting a single anlegg")
        params = None
        if filters:
            items = [" eq ".join((k, str(v))) for k, v in filters.items()]
            params = {"filters": " and ".join(items)}

        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.Begrepsverdi, r)

    def get_koststed(
        self,
        company_id: str,
        koststed: Optional[str] = None,
        filters: Optional[Dict[str, Union[str, int]]] = None,
    ) -> Union[List[models.Begrepsverdi], models.Begrepsverdi, None]:
        """
        Get all or single koststed of company with company_id

        Accepts a dict of filters with the following keys:

        "attributeId", "attributeName", "attributeValue", "companyId",
        "customValue", "owner", "ownerAttributeId", "ownerAttributeName",
        "periodFrom" "periodTo", "status"

        with values as int or str
        """
        url = self.urls.get_koststed(company_id, koststed)
        headers = (
            self.config.rest.endpoints.koststeder.headers
            if self.config.rest
            else dict()
        )
        if koststed and filters:
            raise ValueError("Filters are not allowed when getting a single koststed")
        params = None
        if filters:
            items = [" eq ".join((k, str(v))) for k, v in filters.items()]
            params = {"filters": " and ".join(items)}
        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.Begrepsverdi, r)

    def get_begreper(
        self, company_id: str
    ) -> Union[List[models.Begrep], models.Begrep, None]:
        url = self.urls.get_begreper(company_id)
        headers = (
            self.config.rest.endpoints.begreper.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Begrep, r)

    def get_begrepsverdier(
        self,
        company_id: str,
        begrep_id: Optional[str] = None,
        aktiv: bool = False,
        begrep_verdi: Optional[str] = None,
    ) -> Union[List[models.Begrepsverdi], models.Begrepsverdi, None]:
        url = self.urls.get_begrepsverdier(company_id, begrep_id)
        headers = (
            self.config.rest.endpoints.begrepsverdier.headers
            if self.config.rest
            else dict()
        )
        params = None
        filters = []
        if begrep_verdi:
            filters.append(f"attributeValue eq '{begrep_verdi}'")
        if aktiv:
            filters.append("status eq 'N'")
        if filters:
            params = {"filter": " and ".join(filters)}

        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.Begrepsverdi, r)

    def get_prosjekt(
        self, company_id: str, project_id: str
    ) -> Union[List[models.Prosjekt], models.Prosjekt, None]:
        url = self.urls.get_prosjekter(company_id, project_id)
        headers = (
            self.config.rest.endpoints.project.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Prosjekt, r)

    def get_prosjekter(
        self, company_id: str
    ) -> Union[List[models.Prosjekt], models.Prosjekt, None]:
        url = self.urls.get_prosjekter(company_id)
        headers = (
            self.config.rest.endpoints.project.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Prosjekt, r)

    def post_prosjekter(self) -> None:
        raise NotImplementedError

    def delete_prosjekter(self) -> None:
        raise NotImplementedError

    def patch_prosjekter(self) -> None:
        raise NotImplementedError

    def get_firma(
        self, company_id: str
    ) -> Union[List[models.Firma], models.Firma, None]:
        url = self.urls.get_firma(company_id)
        headers = (
            self.config.rest.endpoints.firma.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Firma, r)

    def get_arbeidsordre(
        self, company_id: str, arbeidsordre_id: str
    ) -> Union[List[models.Arbeidsordre], models.Arbeidsordre, None]:
        url = self.urls.get_arbeidsordre(company_id, arbeidsordre_id)
        headers = (
            self.config.rest.endpoints.arbeidsordre.headers
            if self.config.rest
            else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Arbeidsordre, r)

    def get_arbeidsordrer(
        self, company_id: str
    ) -> Union[List[models.Arbeidsordre], models.Arbeidsordre, None]:
        url = self.urls.get_arbeidsordre(company_id)
        headers = (
            self.config.rest.endpoints.arbeidsordre.headers
            if self.config.rest
            else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Arbeidsordre, r)

    # TODO: Extend get_arbeidsordrer to support the same partial model as the following
    #  function
    def get_partial_arbeidsordrer(
        self, company_id: str
    ) -> Union[List[models.PartialArbeidsordre], models.PartialArbeidsordre, None]:
        url = self.urls.get_arbeidsordre(company_id)
        headers = (
            self.config.rest.endpoints.arbeidsordre.headers
            if self.config.rest
            else dict()
        )
        params = {
            "select": ",".join(
                [
                    "companyId",
                    "workOrderId",
                    "dateFrom",
                    "dateTo",
                    "periodFrom",
                    "periodTo",
                    "status",
                    "projectId",
                    "workOrderName",
                    "costCentre",
                    "projectManagerId",
                    "category1",
                    "lastUpdated(updatedAt,updatedBy)",
                ]
            )
        }

        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.PartialArbeidsordre, r)

    def get_perioder(
        self,
        company_id: str,
        accounting_period: Optional[str] = None,
        period_type: Optional[str] = None,
    ) -> Union[List[models.Periode], models.Periode, None]:
        """Get a list of perioder.

        Filtering by period_type="GL" is applied automatically by the API when
        accounting_period is not None.
        """

        url = self.urls.get_perioder(company_id, accounting_period)
        headers = (
            self.config.rest.endpoints.periode.headers if self.config.rest else dict()
        )

        params = None
        filters = []
        if period_type:
            filters.append(f"periodType/periodType eq '{period_type}'")
        if filters:
            params = {"filter": " and ".join(filters)}

        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.Periode, r)

    def get_periode(
        self, company_id: str, accounting_period: str
    ) -> Optional[models.Periode]:
        """Get a single periode of period_type="GL"."""

        try:
            perioder = self.get_perioder(
                company_id,
                accounting_period=accounting_period,
            )
        except requests.HTTPError as e:
            return none_or_error(e)  # type: ignore[func-returns-value, no-any-return]

        if isinstance(perioder, models.Periode):
            return perioder
        elif isinstance(perioder, list):
            return perioder[0]
        return None

    def get_brukere(
        self, company_id: str
    ) -> Union[List[models.Bruker], models.Bruker, None]:
        url = self.urls.get_bruker(company_id)
        headers = (
            self.config.rest.endpoints.bruker.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Bruker, r)

    def get_bruker(
        self, company_id: str, bruker_id: str
    ) -> Union[List[models.Bruker], models.Bruker, None]:
        url = self.urls.get_bruker(company_id, bruker_id)
        headers = (
            self.config.rest.endpoints.bruker.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Bruker, r)

    def get_gl07logs(
        self, company_id: str, batch_id: Optional[str] = None
    ) -> Union[List[models.GL07Log], models.GL07Log, None]:
        url = self.urls.get_gl07logs(company_id)

        params = {"orderBy": "batchId, voucherNoOrg"}
        if batch_id:
            params["filter"] = f"batchId eq '{batch_id}'"

        headers = (
            self.config.rest.endpoints.gl07logs.headers if self.config.rest else {}
        )
        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.GL07Log, r)

    def get_gl07logchecks(self, company_id: str, batch_id: str) -> Optional[JsonType]:
        url = self.urls.get_gl07logchecks(company_id)

        params = {}
        if batch_id:
            params["filter"] = f"batchId eq '{batch_id}'"

        headers = (
            self.config.rest.endpoints.gl07logchecks.headers if self.config.rest else {}
        )
        return self._get_with_try(url=url, params=params, headers=headers)

    def get_kontoplan(
        self,
        company_id: str,
        account_group: Optional[str] = None,
    ) -> models.Kontoplan:
        """
        Send get request to kontoplan api endpoint

        TODO: Add support for more filters
        """
        url = self.urls.get_kontoplan(company_id)

        headers = {}
        if self.config.rest:
            headers = self.config.rest.endpoints.kontoplan.headers
        params = {}
        if account_group:
            params["filter"] = f"accountGroup eq '{account_group}'"

        try:
            response: Any = self.get(url, params=params, headers=headers)
            accounts = [models.Konto(**account) for account in response]
            return models.Kontoplan(company_id=company_id, accounts=accounts)
        except requests.HTTPError as e:
            return none_or_error(e)  # type: ignore[func-returns-value, no-any-return]

    def get_konto(
        self,
        company_id: str,
        konto: str,
    ) -> Union[List[models.Konto], models.Konto, None]:
        url = self.urls.get_konto(company_id, konto)
        headers = (
            self.config.rest.endpoints.konto.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers, get_first=True)
        return _list_or_obj(models.Konto, r)

    def get_konteringsregler(
        self, company_id: str
    ) -> Union[List[models.Konteringsregel], models.Konteringsregel, None]:
        url = self.urls.get_konteringsregler(company_id)
        headers = (
            self.config.rest.endpoints.konteringsregler.headers
            if self.config.rest
            else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Konteringsregel, r)

    def get_konteringsregel(
        self, company_id: str, konto: int
    ) -> Union[List[models.Konteringsregel], models.Konteringsregel, None]:
        url = self.urls.get_konteringsregel(company_id, konto)
        headers = (
            self.config.rest.endpoints.konteringsregel.headers
            if self.config.rest
            else dict()
        )
        r = self._get_with_try(url=url, headers=headers, get_first=True)
        return _list_or_obj(models.Konteringsregel, r)

    def get_kunder(
        self,
        company_id: str,
        select: Optional[str] = None,
        filter: Optional[str] = None,
        order_by: Optional[str] = None,
        offset: Optional[int] = None,
        limit: Optional[int] = None,
    ) -> Union[List[models.Kunde], models.Kunde, None]:
        url = self.urls.get_kunder(company_id)
        headers = (
            self.config.rest.endpoints.kunder.headers if self.config.rest else dict()
        )

        params: Dict[str, Union[str, int]] = {}
        if select:
            params["select"] = select
        if filter:
            params["filter"] = filter
        if order_by:
            params["orderBy"] = order_by
        if offset:
            params["offset"] = offset
        if limit:
            params["limit"] = limit

        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.Kunde, r)

    def get_kunde(
        self, company_id: str, kunde_id: str
    ) -> Union[List[models.Kunde], models.Kunde, None]:
        url = self.urls.get_kunde(company_id, kunde_id)
        headers = (
            self.config.rest.endpoints.kunder.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        # Single objects may be returned inside a list or as a single object so we check
        # what we got and return it as a Kunde object.
        if isinstance(r, list) and len(r) == 1:
            return _list_or_obj(models.Kunde, r[0])
        return _list_or_obj(models.Kunde, r)

    def get_bilagstyper(
        self, company_id: str
    ) -> Union[List[models.Bilagstype], models.Bilagstype, None]:
        url = self.urls.get_bilagstyper(company_id)
        headers = (
            self.config.rest.endpoints.bilagstyper.headers
            if self.config.rest
            else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Bilagstype, r)

    def get_bilagstype(
        self, company_id: str, ressurs_id: str
    ) -> Union[List[models.Bilagstype], models.Bilagstype, None]:
        url = self.urls.get_bilagstype(company_id, ressurs_id)
        headers = (
            self.config.rest.endpoints.bilagstyper.headers
            if self.config.rest
            else dict()
        )
        r = self._get_with_try(url=url, headers=headers, get_first=True)
        return _list_or_obj(models.Bilagstype, r)

    def get_avgiftskode(
        self,
        company_id: str,
        avgiftskode: Optional[str] = None,
        filters: Optional[Dict[str, Union[str, int]]] = None,
    ) -> Union[List[models.Avgiftskode], models.Avgiftskode, None]:
        """
        Get all or single avgiftskode of company with company_id

        Accepts a dict of filters with the following keys:

        "allowAmendingCalculatedVat", "cashPrinciple", "companyId",
        "currencyCode", "description", "rounding", "roundingClosest",
        "roundingDownwards", "roundingUpwards", "taxCode", "type",
        "updateAmount", "usedInAccountsPayable", "usedInAccountsReceivable",
        "usedInGeneralLedger", "useNetBasedCalculation"

        with values as bool, int or str
        """
        url = self.urls.get_avgiftskode(company_id, avgiftskode)
        headers = (
            self.config.rest.endpoints.avgiftskoder.headers
            if self.config.rest
            else dict()
        )
        if avgiftskode and filters:
            raise ValueError(
                "Filters are not allowed when getting a single avgiftskode"
            )
        params = None
        if filters:
            items = [" eq ".join((k, str(v))) for k, v in filters.items()]
            params = {"filters": " and ".join(items)}

        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.Avgiftskode, r)

    def get_ressurser(
        self, company_id: str
    ) -> Union[List[models.Ressurs], models.Ressurs, None]:
        url = self.urls.get_ressurser(company_id)
        headers = (
            self.config.rest.endpoints.ressurser.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers)
        return _list_or_obj(models.Ressurs, r)

    def get_ressurs(
        self, company_id: str, ressurs_id: str
    ) -> Union[List[models.Ressurs], models.Ressurs, None]:
        url = self.urls.get_ressurser(company_id, ressurs_id)
        headers = (
            self.config.rest.endpoints.ressurser.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers, get_first=True)
        return _list_or_obj(models.Ressurs, r)

    def get_salgsordrestatus(
        self,
        company_id: str,
        salgsordre_nr: str,
    ) -> Union[List[models.Salgsordrestatus], models.Salgsordrestatus, None]:
        """
        Get salgsordrestatus for a salgsordre with a given number.
        """

        url = self.urls.get_salgsordrestatus(company_id)
        params = {}
        if salgsordre_nr:
            params["filter"] = f"text4 eq '{salgsordre_nr}'"

        headers = (
            self.config.rest.endpoints.salgsordrestatus.headers
            if self.config.rest
            else {}
        )

        r = self._get_with_try(url=url, params=params, headers=headers)
        return _list_or_obj(models.Salgsordrestatus, r)

    def get_produkt(
        self, company_id: str, produkt_id: str
    ) -> Union[List[models.Produkt], models.Produkt, None]:
        url = self.urls.get_produkt(company_id, produkt_id)
        headers = (
            self.config.rest.endpoints.produkter.headers if self.config.rest else dict()
        )
        r = self._get_with_try(url=url, headers=headers, get_first=True)
        return _list_or_obj(models.Produkt, r)


def get_client(**config: Any) -> Tuple[UBWSoapClient, UBWRestClient]:
    soap_client = UBWSoapClient(models.UbwClientConfig(**config))
    rest_client = UBWRestClient(models.UbwClientConfig(**config))
    return soap_client, rest_client


def get_soap_client(**config: Any) -> UBWSoapClient:
    soap_client = UBWSoapClient(models.UbwClientConfig(**config))
    return soap_client


def get_rest_client(**config: Any) -> UBWRestClient:
    rest_client = UBWRestClient(models.UbwClientConfig(**config))
    return rest_client
