import datetime
import urllib.parse
from enum import Enum
import requests

import pytest

from ubw_client.client import ClientError
from ubw_client.client import (
    Endpoints,
    IncorrectPathError,
    _create_server_process_result,
    _map_transaction,
    _replace_control_characters,
)
from ubw_client.models import (
    Arbeidsordre,
    AsynchronousOutput,
    Begrep,
    Begrepsverdi,
    Bruker,
    Firma,
    GL07Log,
    GL07Logcheck,
    Konteringsregel,
    Konto,
    Kontoplan,
    Kunde,
    Periode,
    Prosjekt,
    Ressurs,
    ServerProcessResponse,
    ServerProcessResult,
    Transaction,
    Transactions,
    Avgiftskode,
    UbwRestBase,
    Bilagstype,
    Salgsordrestatus,
)


def test_get_anlegg(rest_client, requests_mock, anlegg_many_data, anlegg_single_data):
    # Test get many
    company_id = "72"
    requests_mock.get(f"https://example.com/anlegg/{company_id}", json=anlegg_many_data)
    expected_many = [Begrepsverdi(**i) for i in anlegg_many_data]
    got_many = rest_client.get_anlegg(company_id)
    assert expected_many == got_many

    # Test get single
    anlegg_id = "10000011"
    requests_mock.get(
        f"https://example.com/anlegg/{company_id}/{anlegg_id}", json=anlegg_single_data
    )
    expected_single = [Begrepsverdi(**i) for i in anlegg_single_data]
    got_single = rest_client.get_anlegg(company_id, anlegg_id)
    assert got_single == expected_single


def test_get_koststed(rest_client, requests_mock, koststeder_data):
    # Test get many
    company_id = "72"
    requests_mock.get(
        f"https://example.com/koststeder/v1/{company_id}", json=koststeder_data
    )
    expected_many = [Begrepsverdi(**i) for i in koststeder_data]
    got_many = rest_client.get_koststed(company_id)
    assert expected_many == got_many

    # Test get single
    koststed_id = "20101030"
    requests_mock.get(
        f"https://example.com/koststeder/v1/{company_id}/{koststed_id}",
        json=koststeder_data[:1],
    )
    expected_single = [Begrepsverdi(**i) for i in koststeder_data[:1]]
    single_got = rest_client.get_koststed(company_id, koststed_id)
    assert single_got == expected_single


def test_get_begreper(rest_client, base_url, requests_mock, begrep_data):
    company_id = "71"
    requests_mock.get(
        f"https://example.com/begreper/{company_id}",
        json=begrep_data,
    )
    received = rest_client.get_begreper(company_id)
    expected = [Begrep.from_dict(x) for x in begrep_data]
    assert len(expected) == len(received)


def test_get_begrepsverdier(rest_client, base_url, requests_mock, begrepsverdier_data):
    company_id = "71"
    requests_mock.get(
        f"https://example.com/begrepsverdier/{company_id}",
        json=begrepsverdier_data,
    )
    received = rest_client.get_begrepsverdier(company_id)
    expected = [Begrepsverdi.from_dict(x) for x in begrepsverdier_data]
    assert len(expected) == len(received)


def test_get_begrepsverdier_for_attribute_id(
    rest_client, base_url, requests_mock, begrepsverdier_data
):
    company_id = "71"
    attribute_id = "AH"
    requests_mock.get(
        f"https://example.com/begrepsverdier/{company_id}/{attribute_id}",
        json=begrepsverdier_data,
    )
    received = rest_client.get_begrepsverdier(company_id, attribute_id)
    expected = [Begrepsverdi.from_dict(x) for x in begrepsverdier_data]
    assert len(expected) == len(received)


def test_get_begrepsverdi_for_single_attribute(
    rest_client, base_url, requests_mock, begrepsverdier_data
):
    company_id = "71"
    attribute_id = "AH"
    attribute_value = "999"
    requests_mock.get(
        f"https://example.com/begrepsverdier/{company_id}/{attribute_id}"
        f"?filter=attributeValue eq '{attribute_value}'",
        json=begrepsverdier_data,
    )
    received = rest_client.get_begrepsverdier(
        company_id, attribute_id, False, attribute_value
    )
    expected = [Begrepsverdi.from_dict(x) for x in begrepsverdier_data]
    assert len(expected) == len(received)


def test_get_prosjekt(rest_client, requests_mock, project_data):
    company_id = "71"
    project_id = "110001"
    requests_mock.get(
        f"https://example.com/prosjekter/{company_id}/{project_id}",
        json=project_data,
    )
    received = rest_client.get_prosjekt(company_id, project_id)
    expected = Prosjekt.from_dict(project_data)
    assert expected == received


def test_get_firma(rest_client, base_url, requests_mock, firma_data):
    company_id = "71"
    requests_mock.get(f"https://example.com/firma/{company_id}", json=firma_data)
    received = rest_client.get_firma(company_id)
    expected = Firma.from_dict(firma_data)
    assert expected == received


def test_get_bruker(rest_client, base_url, requests_mock, bruker_data):
    company_id = "72"
    bruker_id = "5"
    requests_mock.get(
        f"https://example.com/brukere/{company_id}/{bruker_id}",
        json=bruker_data,
    )
    received = rest_client.get_bruker(company_id, bruker_id)
    expected = Bruker.from_dict(bruker_data)
    assert expected == received


def test_get_periode(rest_client, base_url, requests_mock, periode_data):
    company_id = "72"
    periode_id = "202008"
    requests_mock.get(
        f"https://example.com/regnskapsperioder/{company_id}/{periode_id}",
        json=periode_data,
    )
    received = rest_client.get_periode(company_id, periode_id)
    expected = Periode.from_dict(periode_data[0])
    assert expected == received


def test_get_periode_empty_from_api(rest_client, base_url, requests_mock, periode_data):
    """Periode detail view return None if API returns empty list."""

    company_id = "72"
    periode_id = "202008"
    requests_mock.get(
        f"https://example.com/regnskapsperioder/{company_id}/{periode_id}",
        json=[],
    )
    assert rest_client.get_periode(company_id, periode_id) is None


def test_get_perioder(rest_client, base_url, requests_mock, periode_data):
    company_id = "72"
    period_type = "GL"
    query_str = urllib.parse.quote(
        f"filter=periodType/periodType eq '{period_type}'".replace(" ", "+"),
        safe="&=+",
    )
    requests_mock.get(
        f"https://example.com/regnskapsperioder/{company_id}?{query_str}",
        json=periode_data,
    )
    received = rest_client.get_perioder(company_id, period_type=period_type)
    expected = [Periode.from_dict(p) for p in periode_data]
    assert expected == received


def test_get_perioder_empty(rest_client, base_url, requests_mock):
    company_id = "72"
    period_type = "GL"
    query_str = urllib.parse.quote(
        f"filter=periodType/periodType eq '{period_type}'".replace(" ", "+"),
        safe="&=+",
    )
    requests_mock.get(
        f"https://example.com/regnskapsperioder/{company_id}?{query_str}",
        json=[],
    )
    received = rest_client.get_perioder(company_id, period_type=period_type)
    assert None is received


def test_get_arbeidsordre(rest_client, base_url, requests_mock, arbeidsordre_data):
    company_id = "72"
    ordre_id = "110000100"
    requests_mock.get(
        f"https://example.com/arbeidsordre/{company_id}/{ordre_id}",
        json=arbeidsordre_data[0],
    )
    received = rest_client.get_arbeidsordre(company_id, ordre_id)
    expected = Arbeidsordre(**arbeidsordre_data[0])
    assert received == expected


def test_get_arbeidsordrer(rest_client, base_url, requests_mock, arbeidsordre_data):
    company_id = "72"
    requests_mock.get(
        f"https://example.com/arbeidsordre/{company_id}",
        json=arbeidsordre_data,
    )
    received = rest_client.get_arbeidsordrer(company_id)
    expected = [Arbeidsordre.from_dict(p) for p in arbeidsordre_data]
    assert received == expected


def test_get_gl07logs(rest_client, base_url, requests_mock, gl07logs_data):
    company_id = "72"
    requests_mock.get(
        f"https://example.com/objekter/gl07logs/{company_id}",
        json=gl07logs_data,
    )
    received = rest_client.get_gl07logs(company_id)
    expected = [GL07Log(**log) for log in gl07logs_data]
    assert received == expected


def test_gl07logchecks(rest_client, base_url, requests_mock, gl07logchecks_data):
    batch_id = "19103133"
    company_id = "72"
    requests_mock.get(
        f"https://example.com/gl07logchecks/{company_id}?filter=batchId+eq+%27{batch_id}%27",
        json=gl07logchecks_data,
    )
    received = rest_client.get_gl07logchecks(company_id, batch_id)
    expected = [GL07Logcheck(**logcheck) for logcheck in gl07logchecks_data]
    assert received == expected


def test_get_kontoplan(rest_client, requests_mock, konto_data):
    company_id = "72"
    requests_mock.get(
        f"https://example.com/kontoplan/v1/{company_id}",
        json=konto_data,
    )
    received = rest_client.get_kontoplan(company_id)
    expected = Kontoplan(
        company_id=company_id, accounts=[Konto(**i) for i in konto_data]
    )
    assert received == expected


def test_get_konto(rest_client, requests_mock, konto_data):
    company_id = "72"
    account_id = "2943"
    requests_mock.get(
        f"https://example.com/kontoplan/v1/{company_id}/{account_id}",
        json=konto_data,
    )
    received = rest_client.get_konto(company_id, account_id)
    expected = Konto(**konto_data[0])
    assert received == expected


def test_get_konteringsregler(rest_client, requests_mock, konteringsregler_data):
    company_id = "72"
    requests_mock.get(
        f"https://example.com/konteringsregler/v1/{company_id}",
        json=konteringsregler_data,
    )
    received = rest_client.get_konteringsregler(company_id)
    expected = [Konteringsregel(**i) for i in konteringsregler_data]
    assert received == expected


def test_get_konteringsregel(rest_client, requests_mock, konteringsregler_data):
    company_id = "72"
    account_id = "2"
    requests_mock.get(
        f"https://example.com/konteringsregler/v1/{company_id}/{account_id}",
        json=konteringsregler_data,
    )
    received = rest_client.get_konteringsregel(company_id, account_id)
    expected = Konteringsregel(**konteringsregler_data[0])
    assert received == expected


def test_get_avgiftskode(rest_client, requests_mock, avgiftskoder_data):
    # Test get many
    company_id = "72"
    requests_mock.get(
        f"https://example.com/avgiftskoder/v1/{company_id}", json=avgiftskoder_data
    )
    expected_many = [Avgiftskode(**i) for i in avgiftskoder_data]
    got_many = rest_client.get_avgiftskode(company_id)
    assert expected_many == got_many

    # Test get single
    avgiftskode = "3P"
    requests_mock.get(
        f"https://example.com/avgiftskoder/v1/{company_id}/{avgiftskode}",
        json=avgiftskoder_data[:1],
    )
    expected_single = [Avgiftskode(**i) for i in avgiftskoder_data[:1]]
    single_got = rest_client.get_avgiftskode(company_id, avgiftskode)
    assert single_got == expected_single


def test_get_transaction_report(soap_client, base_url, transaction_report_data):
    report = _create_server_process_result(
        transaction_report_data["Envelope"]["Body"]["GetResultResponse"][
            "GetResultResult"
        ]
    )

    assert isinstance(report, ServerProcessResult)
    # TODO: What does multiple responses mean?
    assert len(report.response_list) == 1
    assert report.response_list[0] == ServerProcessResponse(status="OK", return_code=10)
    assert report.error_message is None
    assert len(report.file_list) == 2  # TODO: What about len > 2 or len == 1?
    report_file = next(filter(lambda x: x.file_name.endswith(".lis"), report.file_list))
    log_file = next(filter(lambda x: x.file_name.endswith(".log"), report.file_list))
    assert report_file.file_name == "gl07a_72_349336.lis"
    assert log_file.file_name == "gl07_349336.log"
    assert report_file.decoded_file != ""
    assert log_file.decoded_file != ""


def test_get_transaction_report_fails_due_to_wrong_log_file(
    soap_client, base_url, transaction_report_data_faulty
):
    with pytest.raises(
        ValueError,
        match="Invalid ServerProcessResult, missing log files in resp",
    ):
        _create_server_process_result(
            transaction_report_data_faulty["Envelope"]["Body"]["GetResultResponse"][
                "GetResultResult"
            ]
        )


def m_test_save_transaction_without_transaction_service(soap_client):
    with pytest.raises(ClientError) as tx_error:
        soap_client.save_transaction({}, (), 1, 1, "batch_id")
    assert tx_error.value.args[0] == "Transaction API is not configured"


def m_test_check_if_can_delete_batch_input_without_transaction_service(soap_client):
    with pytest.raises(ClientError) as tx_error:
        soap_client.check_if_can_delete_batch_input("batch_id", "interface_id")
    assert tx_error.value.args[0] == "Transaction API is not configured"


def m_test_get_transaction_status_without_import_service(soap_client):
    with pytest.raises(ClientError) as imp_error:
        soap_client.get_transaction_status("GA07", 1)
    assert imp_error.value.args[0] == "Import API is not configured"


def test_ubw_client_config():
    config = UbwRestBase(base_url="https://example.com")
    endpoints = Endpoints(config)
    assert endpoints.get_begreper("72") == "https://example.com/begreper/72"
    assert (
        endpoints.get_begrepsverdier("72", "12")
        == "https://example.com/begrepsverdier/72/12"
    )
    assert (
        endpoints.get_prosjekter("72", "12") == "https://example.com/prosjekter/72/12"
    )
    assert endpoints.get_firma("72") == "https://example.com/firma/72"
    assert (
        endpoints.get_arbeidsordre("72", "12")
        == "https://example.com/arbeidsordre/72/12"
    )
    assert (
        endpoints.get_perioder("72", "12")
        == "https://example.com/regnskapsperioder/72/12"
    )
    assert endpoints.get_bruker("72", "12") == "https://example.com/brukere/72/12"
    assert endpoints.get_gl07logs("72") == "https://example.com/objekter/gl07logs/72"
    assert endpoints.get_kontoplan("72") == "https://example.com/kontoplan/v1/72"
    assert endpoints.get_konto("72", "12") == "https://example.com/kontoplan/v1/72/12"
    assert (
        endpoints.get_konteringsregler("72")
        == "https://example.com/konteringsregler/v1/72"
    )
    assert (
        endpoints.get_konteringsregel("72", 12)
        == "https://example.com/konteringsregler/v1/72/12"
    )
    assert endpoints.get_bilagstyper("72") == "https://example.com/bilagstyper/v1/72"
    assert (
        endpoints.get_bilagstype("72", "TT")
        == "https://example.com/bilagstyper/v1/72/TT"
    )
    assert endpoints.get_ressurser("72") == "https://example.com/ressurser/v1/72"
    assert (
        endpoints.get_ressurser("72", "12") == "https://example.com/ressurser/v1/72/12"
    )


def test_map_transaction():
    transaction = Transaction(
        account="account",
        amount="amount",
        client="client",
        cur_amount=0,
        currency=1,
        description="description",
        dim1="dim1",
        dim2="dim2",
        dim5="dim5",
        dim6="dim6",
        dim7="dim7",
        interface="interface",
        sequence_no=2,
        tax_code="tax_code",
        trans_type="trans_type",
        voucher_no=3,
        voucher_type="voucher_type",
    )

    mapped = _map_transaction(transaction)
    for k, v in transaction.dict().items():
        assert mapped.get(k) == v


def test_map_transaction_null_dates():
    transaction = Transaction(
        account="account",
        amount="amount",
        client="client",
        cur_amount=0,
        currency=1,
        description="description",
        dim1="dim1",
        dim2="dim2",
        dim5="dim5",
        dim6="dim6",
        dim7="dim7",
        interface="interface",
        sequence_no=2,
        tax_code="tax_code",
        trans_type="trans_type",
        voucher_no=3,
        voucher_type="voucher_type",
    )
    mapped = _map_transaction(transaction)
    null_date = datetime.date(year=1900, month=1, day=1)
    assert mapped["ArrivalDate"] == null_date
    assert mapped["TransDate"] == null_date
    assert mapped["VoucherDate"] == null_date
    assert mapped["DiscDate"] == null_date
    assert mapped["DueDate"] == null_date
    assert mapped["ComplDelay"] == null_date


def test_get_bilagstyper(rest_client, requests_mock, bilagstyper_data):
    company_id = "72"
    requests_mock.get(
        f"https://example.com/bilagstyper/v1/{company_id}",
        json=bilagstyper_data,
    )
    received = rest_client.get_bilagstyper(company_id)
    expected = [Bilagstype.from_dict(b) for b in bilagstyper_data]
    assert len(received) == 4
    for i in range(len(received)):
        assert received[i] == expected[i]


def test_get_bilagstype(rest_client, requests_mock, bilagstype_data):
    company_id = "72"
    transaction_type = "TT"
    requests_mock.get(
        f"https://example.com/bilagstyper/v1/{company_id}/{transaction_type}",
        json=bilagstype_data,
    )
    received = rest_client.get_bilagstype(company_id, transaction_type)
    expected = Bilagstype.from_dict(bilagstype_data[0])
    assert expected == received


def test_get_ressurs(rest_client, base_url, requests_mock, ressurs_data):
    company_id = "72"
    ressurs_id = "99999"
    requests_mock.get(
        f"https://example.com/ressurser/v1/{company_id}/{ressurs_id}",
        json=ressurs_data,
    )
    received = rest_client.get_ressurs(company_id, ressurs_id)
    expected = Ressurs.from_dict(ressurs_data[0])
    assert expected == received


def test_get_ressurser(rest_client, base_url, requests_mock, ressurser_data):
    company_id = "72"
    requests_mock.get(
        f"https://example.com/ressurser/v1/{company_id}",
        json=ressurser_data,
    )
    received = rest_client.get_ressurser(company_id)
    expected = [Ressurs.from_dict(b) for b in ressurser_data]
    assert len(received) == 3
    for i in range(len(received)):
        assert received[i] == expected[i]


def test_execute_server_process_asynchronously(
    soap_client,
    requests_mock,
    salgsordre,
    salgsordre_response,
    salgsordre_to_unit4_content,
):
    """
    Ensure client handles input and response correctly for the
    execute_server_process_asynchronously method.
    """
    requests_mock.post(
        "https://example.com/soap/importtjeneste",
        headers={"Content-Type": "text/xml; charset=utf-8"},
        text=salgsordre_response,
    )
    received = soap_client.execute_server_process_asynchronously(
        server_process_id="LG04",
        menu_id="SO103",
        variant=159,
        xml=salgsordre,
    )

    salgsordre_response_object = AsynchronousOutput(
        order_number=39935,
        input_table_name="algbatchinput",
        namespace="http://services.agresso.com/schema/ABWOrder/2007/12/24",
        response_list=[],
    )
    expected = salgsordre_response_object
    assert received.order_number == expected.order_number
    assert received.input_table_name == expected.input_table_name
    assert received.namespace == expected.namespace
    assert received.response_list == expected.response_list
    assert requests_mock.last_request.text == salgsordre_to_unit4_content


def test_get_kunde(rest_client, requests_mock, kunde_data):
    """Ensure method works with list response for single object"""
    requests_mock.get("https://example.com/kunder/v1/72/1", json=kunde_data)
    kunde = rest_client.get_kunde("72", "1")
    assert kunde == Kunde(**kunde_data[0])

    requests_mock.get("https://example.com/kunder/v1/72/2", status_code=404)
    with pytest.raises(IncorrectPathError):
        kunde = rest_client.get_kunde("72", "2")


def test_get_kunde_wrong_path(rest_client, requests_mock, kunde_data):
    """Ensure method works with list response for single object"""
    requests_mock.get(
        "https://example.com/kunder/v1/72/2",
        status_code=404,
        json={"statusCode": 404, "message": "Resource not found"},
    )
    with pytest.raises(IncorrectPathError):
        kunde = rest_client.get_kunde("72", "2")


def test_get_kunde_old_data(rest_client, requests_mock, kunde_old_data):
    """Ensure method works with single object response for single object"""
    requests_mock.get("https://example.com/kunder/v1/72/1", json=kunde_old_data)
    kunde = rest_client.get_kunde("72", "1")
    assert kunde == Kunde(**kunde_old_data)

    requests_mock.get("https://example.com/kunder/v1/72/2", status_code=404)
    with pytest.raises(IncorrectPathError):
        kunde = rest_client.get_kunde("72", "2")


def test_get_kunder(rest_client, requests_mock, kunde_data):
    """Ensure method works for multiple objects"""
    requests_mock.get(
        "https://example.com/kunder/v1/72?select=foo&limit=4", json=kunde_data
    )
    kunder = rest_client.get_kunder("72", limit=4, select="foo")
    assert kunder == [Kunde(**i) for i in kunde_data]


def test_salgsordrestatus(rest_client, base_url, requests_mock, salgsordrestatus_data):
    salgsordre_nr = "128"
    company_id = "72"
    requests_mock.get(
        f"https://example.com/salgsordrestatus/v1/{company_id}?filter=text4+eq+%27{salgsordre_nr}%27",
        json=salgsordrestatus_data,
    )
    received = rest_client.get_salgsordrestatus(company_id, salgsordre_nr)
    expected = [Salgsordrestatus(**logcheck) for logcheck in salgsordrestatus_data]
    assert received == expected


class ControlCharEnum(str, Enum):
    normal = "normal"
    control = "\x00control"


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ("\t\r\n", "\t\r\n"),
        ("A\x00 B\t\r\nC \x1a", "A\ufffd B\t\r\nC \ufffd"),
        (ControlCharEnum.control, "\ufffdcontrol"),
        (ControlCharEnum.normal, ControlCharEnum.normal),
    ],
)
def test__replace_control_characters(test_input, expected):
    assert _replace_control_characters(test_input) == expected


@pytest.mark.parametrize(
    "test_input",
    [
        (b"\x00\x1a",),
        (12.0,),
        (None,),
        (True,),
        (False,),
        (datetime.date(2000, 1, 1),),
    ],
)
def test__replace_control_characters_not_str(test_input):
    assert _replace_control_characters(test_input) is test_input


def test__replace_control_characters_all(ascii_control_characters):
    replaced_ascii_control_characters = _replace_control_characters(
        ascii_control_characters
    )

    assert (
        replaced_ascii_control_characters
        == "\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\t\n"
        "\ufffd\ufffd\r\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd"
        "\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd"
    )


def test_save_transactions_with_invalid_characters(
    soap_client,
    mock_api,
    save_transactions_response,
    mocker,
    ascii_control_characters,
):
    mock_api.post(
        url="https://example.com/soap/batchinputservice",
        text=save_transactions_response,
    )
    mocked__replace_control_characters = mocker.patch(
        "ubw_client.client._replace_control_characters"
    )
    soap_client.save_transactions(
        transactions=Transactions(
            __root__=[
                Transaction(
                    client="",
                    account="",
                    cur_amount="",
                    amount="",
                    description=ascii_control_characters,
                    currency="",
                    interface="",
                    sequence_no=0,
                    trans_type="",
                    voucher_no=0,
                    voucher_type="",
                )
            ]
        ),
        process_parameters=("", ""),
        report_variant=0,
        period=0,
        batch_id="",
    )
    mocked__replace_control_characters.assert_any_call(ascii_control_characters)
