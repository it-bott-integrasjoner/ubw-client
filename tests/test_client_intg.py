# type: ignore

import logging

import pytest
import requests
from ubw_client import models
from ubw_client.client import get_rest_client, get_soap_client
from ubw_client.models import AsynchronousOutput

"""
To run integration tests, set credentials in config.yaml and run

    python -m pytest -m integration
"""


@pytest.mark.integration
def test_save_transactions(service_config):
    client = get_soap_client(**service_config)

    transactions = create_transactions()
    resp = client.save_transactions(transactions, ("interface", "BI"), 0, 202006, "")
    assert resp is not None
    assert resp.order_no > 0
    assert resp.batch_id is not None


@pytest.mark.integration
def test_check_if_can_delete_batch_input(service_config):
    client = get_soap_client(**service_config)
    resp = client.check_if_can_delete_batch_input("19102820", "FA")
    assert resp is not None
    assert resp is True


@pytest.mark.integration
def test_check_if_can_delete_batch_input_2(service_config):
    client = get_soap_client(**service_config)
    resp = client.check_if_can_delete_batch_input("UL0000030", "UL")
    assert resp is not None
    assert resp is True


@pytest.mark.integration
def test_check_if_can_delete_non_existing_batch_input(service_config):
    client = get_soap_client(**service_config)
    resp = client.check_if_can_delete_batch_input("UL0001111", "UL")
    assert resp is not None
    assert resp is False


@pytest.mark.integration
def test_get_status_for_existing_transactions(service_config):
    client = get_soap_client(**service_config)

    status = client.get_transaction_status("GL07", 349171)
    expected_status = "Unknown status:"
    assert expected_status == status


@pytest.mark.integration
def test_get_status_for_new_transactions(service_config):
    client = get_soap_client(**service_config)

    transactions = create_transactions()
    resp = client.save_transactions(transactions, ("interface", "BI"), 0, 202006, "")
    status = client.get_transaction_status("GL07", resp.order_no)
    expected_status = "Finished"
    assert expected_status == status


@pytest.mark.integration
def test_get_transaction_report(service_config):
    client = get_soap_client(**service_config)

    report = client.get_transaction_report("GL07", 349336)

    assert isinstance(report, models.ServerProcessResult)
    # TODO: What does multiple responses mean?
    assert len(report.response_list) == 1
    assert report.response_list[0] == models.ServerProcessResponse(
        status="OK", return_code=10
    )

    assert len(report.file_list) == 2  # TODO: What about len > 2 or len == 1?
    report_file = next(filter(lambda x: x.file_name.endswith(".lis"), report.file_list))
    log_file = next(filter(lambda x: x.file_name.endswith(".log"), report.file_list))
    assert report_file.decoded_file != ""
    assert log_file.decoded_file != ""


@pytest.mark.integration
def test_get_transaction_report_invalid_order_no(service_config):
    client = get_soap_client(**service_config)
    report = client.get_transaction_report("GL07", -1)
    assert isinstance(report, models.ServerProcessResult)
    assert len(report.file_list) == 0
    # TODO: What does multiple responses mean?
    assert len(report.response_list) == 1
    assert report.response_list[0] == models.ServerProcessResponse(
        status="OK", return_code=10
    )


@pytest.mark.integration
def test_get_firma(service_config):
    client = get_rest_client(**service_config)
    firma = client.get_firma("72")
    assert firma
    assert firma.vat_registration_number == "970422528MVA"


@pytest.mark.integration
def test_get_firma_not_found(service_config):
    client = get_rest_client(**service_config)
    with pytest.raises(requests.exceptions.HTTPError):
        _ = client.get_firma("NOPE")


@pytest.mark.integration
def test_get_begrepsverdier(service_config):
    client = get_rest_client(**service_config)
    begrepsverdier = client.get_begrepsverdier("72", "AH")
    assert len(begrepsverdier) > 3
    assert begrepsverdier is not None


@pytest.mark.integration
def test_get_begrepsverdier_for_attribute_id(service_config):
    client = get_rest_client(**service_config)
    begrepsverdier = client.get_begrepsverdier("72", "AH")
    assert len(begrepsverdier) > 3
    assert begrepsverdier is not None


@pytest.mark.integration
def test_get_single_begrepsverdi(service_config):
    client = get_rest_client(**service_config)
    begrepsverdier = client.get_begrepsverdier("72", "AH", begrep_verdi=999)
    assert len(begrepsverdier) == 1
    assert begrepsverdier[0].description == "Hjelpekonti"


@pytest.mark.integration
def test_get_brukere(service_config):
    client = get_rest_client(**service_config)
    brukere = client.get_brukere("72")
    assert brukere


@pytest.mark.integration
def test_get_bruker(service_config):
    client = get_rest_client(**service_config)
    bruker = client.get_bruker("72", "ØKONOM")
    assert bruker


@pytest.mark.integration
def test_get_perioder(service_config):
    client = get_rest_client(**service_config)
    perioder = client.get_perioder("72")
    assert perioder
    for p in perioder:
        assert p.company_id == "72"
        assert p.accounting_period
        assert p.period_type.period_type


@pytest.mark.integration
def test_get_perioder_filter_type(service_config):
    client = get_rest_client(**service_config)
    perioder = client.get_perioder("72", period_type="GL")
    assert perioder
    for p in perioder:
        assert p.company_id == "72"
        assert p.accounting_period
        assert p.period_type.period_type == "GL"


@pytest.mark.integration
def test_get_perioder_filter_period(service_config):
    client = get_rest_client(**service_config)
    perioder = client.get_perioder("72", accounting_period="202001")
    assert perioder
    for p in perioder:
        assert p.company_id == "72"
        assert p.accounting_period
        assert p.period_type.period_type


@pytest.mark.integration
def test_get_perioder_filter_period_type(service_config):
    client = get_rest_client(**service_config)
    perioder = client.get_perioder("72", accounting_period="202001", period_type="GL")
    assert perioder
    assert len(perioder) == 1
    for p in perioder:
        assert p.company_id == "72"
        assert p.accounting_period == "202001"
        assert p.period_type.period_type == "GL"


@pytest.mark.integration
def test_get_periode(service_config):
    client = get_rest_client(**service_config)
    periode = client.get_periode("72", "202001")
    assert periode
    assert periode.company_id == "72"
    assert periode.accounting_period == "202001"
    assert periode.period_type.period_type == "GL"


@pytest.mark.integration
def test_get_periode_not_found(service_config):
    client = get_rest_client(**service_config)
    assert not client.get_perioder("72", period_type="NOPE")


@pytest.mark.integration
def test_get_gl07logs(service_config):
    client = get_rest_client(**service_config)
    logs = client.get_gl07logs("72")
    assert logs


@pytest.mark.integration
def test_get_gl07logs_filter(service_config):
    client = get_rest_client(**service_config)
    logs = client.get_gl07logs("72", "FOF12345671")
    assert logs
    for log in logs:
        assert log.batch_id == "FOF12345671"


@pytest.mark.integration
def test_get_arbeidsordre(service_config):
    client = get_rest_client(**service_config)
    arbeidsordre = client.get_arbeidsordre("72", "110000100")
    assert arbeidsordre
    assert arbeidsordre.company_id == "72"
    assert arbeidsordre.related_values[0].relation_name


@pytest.mark.integration
def test_get_arbeidsordre_aopartner(service_config):
    client = get_rest_client(**service_config)
    arbeidsordre = client.get_arbeidsordre("72", "201002101")
    assert arbeidsordre
    assert arbeidsordre.company_id == "72"
    assert arbeidsordre.related_values[0].relation_name
    assert hasattr(arbeidsordre.custom_field_groups, "aopartner")


@pytest.mark.integration
def test_get_arbeidsordrer(service_config):
    client = get_rest_client(**service_config)
    arbeidsordre = client.get_arbeidsordrer("72")
    assert len(arbeidsordre) > 3
    assert arbeidsordre[0].company_id == "72"
    assert arbeidsordre[0].related_values[0].relation_name


@pytest.mark.integration
def test_get_prosjekt(service_config):
    client = get_rest_client(**service_config)
    prosjekt = client.get_prosjekt("72", "110002")
    assert prosjekt
    assert prosjekt.company_id == "72"
    assert prosjekt.related_values[0].relation_name


@pytest.mark.integration
def test_get_prosjekter(service_config):
    client = get_rest_client(**service_config)
    prosjekt = client.get_prosjekter("72")
    assert len(prosjekt) > 3
    assert prosjekt[0].company_id == "72"
    assert prosjekt[0].related_values[0].relation_name


@pytest.mark.integration
def test_get_bilagstyper(service_config):
    client = get_rest_client(**service_config)
    bilagstyper = client.get_bilagstyper("72")
    assert len(bilagstyper) > 3
    for bilagstype in bilagstyper:
        assert bilagstype.companyId == "72"


@pytest.mark.integration
def test_get_bilagstype(service_config):
    transaction_type = "TT"
    client = get_rest_client(**service_config)
    bilagstype = client.get_bilagstype("72", transaction_type)
    assert bilagstype
    assert bilagstype["transactionSeries"] == "TT"
    assert bilagstype["transactionType"] == transaction_type


def create_transactions():
    trans_list = [
        {
            "account": "3030",
            "amount": "456",
            "arrival_date": "2020-06-23",
            "compl_delay": "2020-06-23",
            "disc_date": "2020-06-23",
            "due_date": "2020-06-23",
            "trans_date": "2020-06-23",
            "voucher_date": "2020-06-23",
            "client": "72",
            "cur_amount": "456",
            "currency": "NOK",
            "description": "BatchInput Test",
            "dim1": "20000000",
            "dim2": "110000",
            "dim5": "110000100",
            "interface": "BI",
            "sequence_no": 0,
            "tax_code": "0",
            "trans_type": "GL",
            "voucher_no": "11104",
            "voucher_type": "HB",
            "ex_ref": "7272GK1-OS",
        },
        {
            "account": "3030",
            "amount": "789",
            "arrival_date": "2020-06-23",
            "compl_delay": "2020-06-23",
            "disc_date": "2020-06-23",
            "due_date": "2020-06-23",
            "trans_date": "2020-06-23",
            "voucher_date": "2020-06-23",
            "client": "72",
            "cur_amount": "789",
            "currency": "NOK",
            "description": "BatchInput Test",
            "dim1": "20000000",
            "dim2": "110000",
            "dim5": "110000100",
            "interface": "BI",
            "sequence_no": 0,
            "tax_code": "0",
            "trans_type": "GL",
            "voucher_no": "111123",
            "voucher_type": "HB",
            "ex_ref": "7272GK1-OS",
        },
    ]

    return models.Transactions(__root__=trans_list)


@pytest.mark.integration
def test_get_ressurser(service_config):
    client = get_rest_client(**service_config)
    ressurser = client.get_ressurser("72")
    assert len(ressurser) > 3
    for ressurs in ressurser:
        assert ressurs.companyId == "72"


@pytest.mark.integration
def test_get_ressurs(service_config):
    person_id = "99999"
    client = get_rest_client(**service_config)
    ressurs = client.get_ressurs("72", person_id)
    assert ressurs
    assert ressurs["personName"] == "Dummy, Dummy"
    assert ressurs["personId"] == person_id


@pytest.mark.integration
def test_save_salgs_order(service_config, salgsordre, salgsordre_response):
    client = get_soap_client(**service_config)

    received = client.execute_server_process_asynchronously(
        server_process_id="LG04",
        menu_id="SO103",
        variant=159,
        xml=salgsordre,
    )

    salgsordre_response_object = AsynchronousOutput(
        order_number=39935,
        input_table_name="algbatchinput",
        namespace="http://services.agresso.com/schema/ABWOrder/2007/12/24",
        response_list=[],
    )

    expected = salgsordre_response_object
    assert received.order_number
    assert received.input_table_name == expected.input_table_name
    assert received.namespace == expected.namespace
    assert received.response_list == expected.response_list


def add_logging():
    logging.config.dictConfig(
        {
            "version": 1,
            "formatters": {"verbose": {"format": "%(name)s: %(message)s"}},
            "handlers": {
                "console": {
                    "level": "DEBUG",
                    "class": "logging.StreamHandler",
                    "formatter": "verbose",
                },
            },
            "loggers": {
                "zeep.transports": {
                    "level": "DEBUG",
                    "propagate": True,
                    "handlers": ["console"],
                },
            },
        }
    )


add_logging()
