from locust import HttpUser, TaskSet, task, between
import bios  # type: ignore[import-untyped]
import logging

logger = logging.getLogger(__name__)

ubw_config = bios.read("config.yaml")
endpoints_list = ubw_config["rest"]["endpoints"]
headers = ubw_config["rest"]["headers"]
min_waiting_time = ubw_config["locust_waiting_time"]["min_time"]
max_waiting_time = ubw_config["locust_waiting_time"]["max_time"]


def get_endpoint(endpoint):
    return "/" + endpoints_list[endpoint]["url"] + "/"


class WebsiteTasks(TaskSet):
    @task(10)  # weights can be adjusted in prod
    def check_get_arbeidsordre(self):
        self.client.get(get_endpoint("arbeidsordre") + "UB/102454100", headers=headers)

    @task(10)
    def check_get_anlegg(self):
        self.client.get(get_endpoint("anlegg") + "UB", headers=headers)

    @task(10)
    def check_get_koststed(self):
        self.client.get(get_endpoint("koststeder") + "UB", headers=headers)

    @task(2)
    def check_get_begreper(self):
        self.client.get(get_endpoint("begreper") + "UB", headers=headers)

    @task(10)
    def check_get_begrepsverdier(self):
        self.client.get(get_endpoint("begrepsverdier") + "UB/AH", headers=headers)

    @task(10)
    def check_get_prosjekt(self):
        self.client.get(get_endpoint("project") + "UB/100000", headers=headers)

    @task(1)
    def check_get_firma(self):
        self.client.get(get_endpoint("firma") + "UB", headers=headers)

    @task(5)
    def check_get_brukere(self):
        self.client.get(get_endpoint("bruker") + "UB", headers=headers)

    @task(10)
    def check_get_bruker(self):
        self.client.get(get_endpoint("bruker") + "UB/3780B6057255", headers=headers)

    @task(10)
    def check_get_periode(self):
        self.client.get(get_endpoint("periode") + "UB/202001", headers=headers)

    @task(2)
    def check_get_perioder(self):
        self.client.get(
            get_endpoint("periode") + "UB?periodType/periodType+eq+GL", headers=headers
        )

    @task(2)
    def check_get_arbeidsordrer(self):
        self.client.get(get_endpoint("arbeidsordre") + "UB", headers=headers)

    @task(10)
    def check_get_specific_anlegg(self):
        self.client.get(get_endpoint("anlegg") + "UB/102454100", headers=headers)

    @task(10)
    def check_get_gl07logs(self):
        self.client.get(get_endpoint("gl07logs") + "UB", headers=headers)

    @task(5)
    def check_get_kontoplan(self):
        self.client.get(get_endpoint("kontoplan") + "UB", headers=headers)

    @task(10)
    def check_get_konto(self):
        self.client.get(get_endpoint("kontoplan") + "UB/9466", headers=headers)

    @task(5)
    def check_get_konteringsregler(self):
        self.client.get(get_endpoint("konteringsregler") + "UB", headers=headers)

    @task(10)
    def check_get_konteringsregel(self):
        self.client.get(get_endpoint("konteringsregel") + "UB/30", headers=headers)

    @task(10)
    def check_get_avgiftskode(self):
        self.client.get(get_endpoint("avgiftskoder") + "UB", headers=headers)

    @task(5)
    def check_get_bilagstyper(self):
        self.client.get(get_endpoint("bilagstyper") + "UB", headers=headers)

    @task(10)
    def check_get_bilagstype(self):
        self.client.get(get_endpoint("bilagstyper") + "UB/TT", headers=headers)

    @task(5)
    def check_get_ressursere(self):
        self.client.get(get_endpoint("ressurser") + "UB", headers=headers)

    @task(10)
    def check_get_ressurs(self):
        self.client.get(get_endpoint("ressurser") + "UB/6060620", headers=headers)


class WebsiteUser(HttpUser):
    tasks = [WebsiteTasks]
    wait_time = between(min_waiting_time, max_waiting_time)
